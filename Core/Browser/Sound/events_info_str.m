function [info, known_tags] = events_info_str(par, fields, opt)

% events_info_str - create info string for list of browser events
% ---------------------------------------------------------------
%
% [info, known_tags] = events_info_str(par, fields, opt)
%
% Input:
% ------
%  par - browser handle
%  fields - fields to display
%  opt - list options
%
% Output:
% -------
%  info - cell array of event strings

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
%
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 5673 $
% $Date: 2006-07-11 17:21:35 -0400 (Tue, 11 Jul 2006) $
%--------------------------------
% History
%	crp - 11 Sep 2013
%		Modify layout of Event palette so that columns line up in event	string
%	msp - 28 Dec 2013
%		Stop converting event score of NaN to 0.01.  No reason to invent a score.
%   msp2 - 13 Mar 2014
%       Improve readibility of events strings.
%   msp2 - 14 Mar 2014
%       Replace string delimiters '#' and ':' in event string in Event
%       palette with a tab, aka char(9), which is non-printing in listbox.
%       This prevents '#' in log name from breaking Event palette.  It also
%       fixes a bug introduced in the last revision.
%	msp2 - 18 Mar 2014
%		Fix bug active when processing logs with no events.
%   msp2 - 18 Mar 2014
%       Replace score of 'NaN%' with '     ' for readibility and column
%       alignment.
%   msp2 - 18 Mar 2014
%       Reinstate CRP addition of pipes by event tags to make searches more
%       powerful.
%	msp2 - 20 Mar 2014
%		Unfortunately map_time handles times differently when time grid is set
%		to "Date and Time", so "Clock" and "Seconds" are dealt with here.  Better
%		to do this in map_time.
%   msp2 - 23 Apr 2014
%       Align scores correctly when log has no scores.
%   msp2 - 3 May 2014
%       Add option for sorting Event palette events list by tag.
%   msp2 - 5 May 2014
%       Move ratings to left of tag pipes, so they don't interfere with tag
%       position.
%   msp2 - 17 May 2014
%       Fix bug that prevented rating from being displayed when there was
%         only one event in log.
%       Fix five characters for rating display, no matter what ratings.

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% set and possibly output default options
%--

if (nargin < 3) || isempty(opt)
    
    opt.page = 0; opt.visible = 0; opt.order = 'name';
    
    if ~nargin
        info = opt; return;
    end
    
end

%--
% check sort order value
%--

orders = {'name', 'tag', 'score', 'time', 'rating'};

if ~ismember(opt.order, orders)
    error(['Unrecognized sort order ''', opt.order, '''.']);
end

%--
% check browser input
%--

if ~is_browser(par)
    error('Input handle is not browser handle.');
end

%--
% set default fields
%--

if (nargin < 2) || isempty(fields)
    
    fields = { ...
        'score', ...
        'rating', ...
        'label', ...
        'channel', ...
        'time' ...
        };
end

%----------------------------
% SETUP
%----------------------------

%--
% get browser state
%--

data = get_browser(par);

%--
% create convenience variables for parts of state
%--

log   = data.browser.log;

sound = data.browser.sound;

grid  = data.browser.grid;

%----------------------------
% KEEP VISIBLE
%----------------------------

%--
% select only displayed channels
%--

% NOTE: it makes sense to use visibility filtering all the time

if opt.visible
    
    %--
    % select remove invisible logs
    %--
    
    log(~[log.visible]) = [];
    
    if isempty(log)
        info = []; 
        known_tags = {};
        return;
    end
    
    %--
    % select visible events
    %--
    
    channel = get_channels(data.browser.channels);
    
    if length(channel) < sound.channels
        
        for k = 1:length(log)
            
            %--
            % get event channels and check which are visible
            %--
            
            event_channel = [log(k).event.channel];
            
            %--
            % find visible event indices
            %--
            
            ix = zeros(size(event_channel));
            
            for j = 1:length(channel)
                ix = ix | (event_channel == channel(j));
            end
            
            ix = find(ix);
            
            %--
            % update log copy
            %--
            
            log(k).event = log(k).event(ix); log(k).length = length(ix);
            
        end
        
    end
    
end

%----------------------------
% REMOVE EMPTY LOGS
%----------------------------
for k = length(log):-1:1
    if isempty(log(k).event)
        log(k) = [];
    end
end
if isempty( log )
    info = []; 
    known_tags = {}; 
    return;
end

%----------------------------
% GET KNOWN TAGS
%----------------------------

% TODO: factor this whenever we come across it again

known_tags = {};

for k = 1:length(log)
    known_tags = union(known_tags, unique_tags(log(k).event));
end

%--
% remove empty tags
%--

known_tags = setdiff(known_tags, {''});

for k = 1:length(known_tags)
    
    if known_tags{k}(1) == '*'
        known_tags{k}(1) = [];
    end
    
end

%----------------------------
% CREATE STRINGS
%----------------------------

%--
% order logs if sort order is name
%--

if strcmp(opt.order, 'name')
    [ignore, ix] = sort({log.file}); log = log(ix);
end

%--
% loop over logs to generate event strings
%--

% NOTE: putting strings in cell arrays preserves whitespace in 'strcat'
info = cell(0);

%make format string to set log name string to constant length
log_names = log_name( log );
if ~iscell( log_names )
    log_names = { log_names };
end
fn_len = cellfun( @length, log_names , 'UniformOutput', false );
max_len = max( [ fn_len{ : } ] );
f = sprintf( '%%-%ds', max_len );

%find largest event id to consider
max_id = 0;
for k = 1:length( log )
    max_id = max( [ max_id, log( k ).event( end ).id ] );
end
max_id = max( 100, 10 * ( max_id + 1 ) );

%initalizations
score_exists   = 0;
channel_exists = 0;
time_exists    = 0;
level_exists   = 0;
label_exists   = 0;
rating_exists  = 0;

for k = 1:length(log)
    
	%-----------------------------------------------
	% PREFIX STRING (INVARIANT FROM LINE TO LINE)
	%-----------------------------------------------
    curr_log = log( k );
    id_str = int_to_str( struct_field( curr_log.event, 'id' ), max_id, ' ' );
    fn = sprintf( f, file_ext( curr_log.file ) );
    tab = { sprintf( '\t' ) };
    SK = strcat( { fn }, { ' ' }, tab, { ' ' }, id_str, tab );

    %-----------------
    % FIELDS STRINGS
    %-----------------    
    for j = 1:length(fields)        
        switch fields{j}
            
            %-----------------
            % SCORE
            %-----------------            
            case 'score'                
                % NOTE: we display score as fractional percent                
                score = struct_field( log( k ).event, 'score' );                
                score = round( 1000 * score ) / 10;
                
                %format score
                score_part = num2str( score, '%4.1f%%' );
                
                %if there are only NaN, pad with one leading space for alignment
                if ~isempty( score_part) && isequal( size( score_part, 2), 4 )
                    score_part = [ repmat( ' ', [ size( score_part, 1 ) 1 ] ), score_part ];
                end
                
                %replace 'NaN%' with '     ' readability and column alignment
                score_part = cellstr( score_part );
                score_part = regexprep( score_part, 'NaN%', '    ' );
                
                score_exists = 1;
                
            %-----------------
            % LEVEL
            %-----------------
            case 'level'
                
                level_part = strcat( { 'L= ' }, int_to_str( struct_field( log( k ).event, 'level' ) ) );
                level_exists = 1;
                
            %-----------------
            % CHANNEL
            %-----------------                
            case 'channel'                
                channel_part = strcat({'Ch= '}, ...  % CRP 20091105
                    int_to_str(struct_field(log(k).event, 'channel'), 99));
                channel_exists = 1;
                
            %-----------------
            % TIME
            %-----------------
            case 'time'
                %--
                % get values
                %--
                t = struct_field(log(k).event, 'time');
                t = t(:, 1);
                
                %--
                % map from record time to real time
                %--
                snd = sound;
                if has_sessions_enabled( snd ) && snd.time_stamp.collapse && ~strcmpi( grid.time.labels, 'date and time' )
                    snd.time_stamp = [];
                end
                t = map_time( snd, 'real', 'record', t );

                %--
                % get string
                %--
                time_part = strcat( {'T= '}, get_grid_time_string( grid, t, sound.realtime ) );
                time_exists = 1;

            %-----------------
            % LABEL
            %-----------------

            % TODO: implement label using marked tags,
            %  here and in a few other places

            case 'label'
                label_part = get_label(log(k).event);
                
                if iscell(label_part)
                    label_part = label_part';
                else
                    label_part = cellstr(label_part);
                end
                
                % create logical array; 1 = empty tag
                label_empty = strcmp(label_part, '');
                
                if label_empty
                    label_size = 1;
                else
                    label_size = size(label_part);
                end
                
                % initialize two character arrays of size equal to the tag list
                pipes  = char(zeros(label_size) + '|');
                spaces = char(zeros(label_size) + ' ');
                separators = pipes;
                
                % keep the pipe before empty tag change to space char before non-empty tag
                separators(label_empty==0) = ' ';
                separators = char(separators);
                label_part = char(label_part);
                
                % this suppresses an error about concat'ing an empty
                %  array in the next step
                if label_empty
                    label_part = ' ';
                end
                
                % put a pipe out for all to separate tags from
                %  previous fields
                % then apply the separator (pipe or space)
                % then add another space for legibility
                % then tack on the tags (or empty tags)
                labels = [pipes separators spaces label_part];
                
                % convert char array back to cell
                label_part = cellstr(labels);
                
                label_exists = 1;

            %-----------------
            % RATING
            %-----------------                
            case 'rating'
                
                rating = struct_field(log(k).event, 'rating');
                
                rating(isnan(rating)) = 0;
                
                rating_part = str_line(rating, '*');
                if ~iscell( rating_part )
                    rating_part = { rating_part };
                end
                
                rating_part = strcat(rating_part, str_line(5 - rating, ' '));
                
%                 for r = 1:length(rating_part)
%                     if ~isequal(rating_part{r}, '     ')
                        rating_exists = 1;
%                     end
%                 end                
        end  % switch
        
    end  % fields for-loop
    
    %-----------------
    % CONCATENATE
    %-----------------
    if score_exists
        SK = strcat(SK, {'   '});
        SK = strcat(SK, score_part);
    end
    if channel_exists
        SK = strcat(SK, {'   '});
        SK = strcat(SK, channel_part);
    end
    if time_exists
        SK = strcat(SK, {'   '});
        SK = strcat(SK, time_part);
    end
    if rating_exists
        SK = strcat(SK, {'   '});
        SK = strcat(SK, rating_part);
    end
    if label_exists
        SK = strcat(SK, {'   '});
        SK = strcat(SK, label_part);
    end
    if level_exists
        SK = strcat(SK, {'   '});
        SK = strcat(SK, level_part);
    end
    
    score_exists   = 0;
    channel_exists = 0;
    time_exists    = 0;
    level_exists   = 0;
    label_exists   = 0;
    rating_exists  = 0;
    
    %--
    % append event strings
    %--    
    info = { info{ : }, SK{ : } };
    
end  % log event list for-loop

% NOTE: return if there is nothing to sort

if isempty(info)
    info = {}; return;
end

%----------------------------
% SORT STRINGS
%----------------------------

%--
% get sorting information
%--

% NOTE: time mapping is not required since sessions are monotone

tags = ''; score = []; rating = []; channel = []; time = [];

for k = 1:length(log)
    
    % NOTE: the comma-separated list approach does not handle nan values
    
    label_part = get_label(log(k).event);
    if iscell(label_part)
        label_part = label_part';
    else
        label_part = cellstr(label_part);
    end
    tags = [ tags ; label_part ];

    part = struct_field(log(k).event, 'score'); score = [score; part];
    
    part = struct_field(log(k).event, 'rating'); rating = [rating; part];
    
    part = [log(k).event.channel]'; channel = [channel; part];
    
    time = [time; reshape([log(k).event.time]', 2, [])'];
    
end

time = time(:, 1);

%--
% sort to compute order and order strings
%--

if ~strcmp(opt.order, 'name')
    
    switch opt.order
        case 'tag'
            [ ignore, ix ] = sortrows( tags, 1 );
        
        case 'score'
            [ignore, ix] = sortrows([score, time, channel], [-1, 2, 3]);
            
        case 'rating'
            [ignore, ix] = sortrows([rating, time, channel], [-1, 2, 3]);
            
        case 'time'
            [ignore, ix] = sortrows([time, channel, score], [1, 2, -3]);
            
    end
    
    info = info(ix);
    
end
