function set_mode_slider( pal )
% set_mode_slider - enable/disable frequency sliders depending on mode
%   input
%       pal - palette handle

% History
%   msp2 - 11 June 2014
%       Disable Duration control in "Tag" mode and "Select + Log" mode
            
    % find handles for frequency sliders and associated edit boxes
    h1 = findobj( pal, 'Type', 'uicontrol', 'Style', 'edit' );
    h2 = findobj( pal, 'Type', 'uicontrol', 'Style', 'slider' );
    h = [ h1 ; h2 ];
    tag = get( h, 'Tag' );
    h_dur = h( strcmp( tag, 'duration' ) );
    h_freq1 = h( strcmp( tag, 'min_freq' ) );
    h_freq2 = h( strcmp( tag, 'max_freq' ) );
    h_bandwidth = h( strcmp( tag, 'bandwidth' ) );
    h = [ h_dur ; h_freq1 ; h_freq2 ; h_bandwidth ];

    % disable frequency sliders and associated edit boxes
    set( h, 'Enable', 'off' )

    % find selected mode
    h_mode = findobj( pal, 'Type', 'uicontrol', 'Style', 'listbox', 'Tag', 'mode' ); 
    str = get( h_mode, 'String' );
    val = get( h_mode, 'Value' );
    mode_str = str{ val };

    % enable/disable frequency controls depending on mode
    switch mode_str
        case { 'Select + Log', 'Tag' }
            % nothing to do

        case { 'One Click' }

            % enable duration, and max and min frequency
            set( [ h_dur ; h_freq1 ; h_freq2 ], 'Enable', 'on' )

        case { 'One Click Frequency Tracking' }

            % enable duration and bandwidth
            set( [ h_dur ; h_bandwidth ], 'Enable', 'on' )
    end
    
