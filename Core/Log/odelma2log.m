function log = odelma2log( log )
% odelma2log - If log is oDeLMA output, convert to XBAT log
% ---------------------------------------------------------------
%
% log = odelma2log( log )
%
% Input:
% ------
%  log - may be oDeLMA output or XBAT log
%
% Output:
% -------
%  log - XBAT log

    % if log is not a structure variable (like all XBAT logs are)
    if ~isstruct( log )
        
        % if xbat_log class is available (ie, oDeLMA\ASE_Utils is pathed up)
        if exist('ASE.xbat_log','class')
            
            % if log is oDeLMA xbat_log class
            if isa( log, 'ASE.xbat_log' )
    % %                     f = ASE.obj2log( f );
                %convert oDeLMA output to XBAT log
                log = log.ToStruct;
            end
            
        % log is not an XBAT log, but xbat_log class is not available
        else
            t1 = 'If the log you are attempting to open was produced by ';
            t2 = 'RavenX you must have .\RavenX\oDeLMA\ASE_Utils in your ';
            t3 = 'MATLAB search paths to read it.';
            t = sprintf( '%s\n', t1, t2, t3 );
            fprintf( 2, '\n\n*******************************************************' );
            fail( t, 'WARNING' )
            fprintf( 2, '*******************************************************\n\n' );
        end
    end