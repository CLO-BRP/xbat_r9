function log = raven_save( varargin )
%Create Raven selection table in XBAT
%
%This function has two modes:
%
%  1. "set_up" mode outputs a stripped-down XBAT log variable with no events,
%     which can be used to call this function in "create" mode.
%
%     Input
%
%       outpath - path that selection table will be written to in "create" mode
%
%       fn - name of selection table will be written to in "create" mode
%
%       author - string with author name
%
%       sound - XBAT sound struct
%
%     Output
%
%       log - stripped down XBAT that can be used by this function in "create_mode".
%
%
%
%  2. "create mode" writes a Raven selection table using an XBAT log variable.  
%
%     Input
%
%       log - An XBAT log struct
%
%     Note: "log" can be a log struct created by any XBAT function.
%     If the XBAT log struct was created by raven_save in "set_up" mode, 
%     a struct vector of XBAT events must be added to log.event if the 
%     output Raven selection table is to have selections in it, log.length
%     must be set to number of elements in event vector, and log.curr_id 
%     must be set to to one higher than the maximum log.event().id.

% History
%   msp2 - 12 May 2014
%       Create.
%   msp2 -  9 Mar 2015
%       After consulting with Bobbi about current protocols
%           Remove columns from output
%             End Path
%             End File Offset
%             Real Start Datetime (yyyymmdd_HHMMSS)
%             Real Stop Datetime (yyyymmdd_HHMMSS)
%           Add columns to output
%             Begin Date
%             Begin Hour
%             XBAT Date (not modified automatically by Raven)
%             XBAT Time (not modified automatically by Raven)
%             XBAT Notes
%   msp2 - 10 Mar 2015
%       Remove "XBAT" from column names.
%   msp2 - 18 Mar 2015
%       Add "Begin File" column per Liz's request.
%       Create an empty Raven selection table if there are no events.

%--------------------------------------------------------------------------
%determine whether raven_save is being called in "set_up" mode or "create" mode
%--------------------------------------------------------------------------
if nargin == 1
    log = varargin{ 1 };
    create( log );
elseif nargin == 4
    log = [];
    outpath = varargin{ 1 };
    fn      = varargin{ 2 };
    author  = varargin{ 3 };
    sound   = varargin{ 4 };
    log = set_up( outpath, fn, author, sound );
    create( log );
else
    error( 'Incorrect number of input parameters in call to "raven_save"' );
end


function log = set_up( outpath, fn, author, sound )
  
    %make stripped-down XBAT log with minimal information for making Raven selection table
    log.type = 'selection table';
    log.path = outpath;
    log.file = fn;
    log.author = author;
    log.sound = sound;
    log.event = empty( event_create );
	log.length = 0;                          
    log.curr_id = 1;
    log.userdata = [];
% 	log.userdata.detect.max_num_events = []; %disable maximum log size feature


function create( log )
    %--------------------------------------------------------------------------
    %set sound attributes for real time calculations
    %--------------------------------------------------------------------------

    %deal with 'file' type sounds
    if iscell( log.sound.file )
        file = log.sound.file{ 1 };
    else
        file = log.sound.file;
    end

    %make sound struct with added date-time and time-stamps attributes
    log.sound.realtime = file_datenum( file );
    time_stamps.table = get_schedule_from_files( log.sound, 'yyyymmdd_HHMMSS' );
    [ ~, ixs ] = sort( time_stamps.table( :, 1 ) );
    time_stamps.table = time_stamps.table( ixs, : );
    time_stamps.enable = 1;
    time_stamps.collapse = 1;
    log.sound.time_stamp = time_stamps;

    %make sound struct with time attributes unset (useful for finding file offsets)
    fixed_sound = log.sound;
    fixed_sound.time_stamp = [];
    fixed_sound.attributes = [];

    %--------------------------------------------------------------------------
    %open selection table
    %--------------------------------------------------------------------------
    path_full = fullfile( log.path, [ log.file '.selections.txt' ] );
    fid = fopen( path_full, 'w' );

    %--------------------------------------------------------------------------
    %write selection table header
    %--------------------------------------------------------------------------
    field_names = { 'Selection', ...
                    'View', ...
                    'Channel', ...
                    'Begin Time (s)', ...
                    'End Time (s)', ...
                    'Low Freq (Hz)', ...
                    'High Freq (Hz)', ...
                    'Begin Date', ...
                    'Begin Hour', ...
                    'Begin Path', ...
                    'Begin File', ...
                    'File Offset (s)', ...
                    'Date Check', ...
                    'Time Check', ...
                    'Score', ...
                    'Tags', ...
                    'Notes', ...
                  };
    fprintf( fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n', field_names{ : } );

    %--------------------------------------------------------------------------
    %write selection table body
    %--------------------------------------------------------------------------
    badTimestampFiles = {};
    flag = false;
    for i = 1:log.length
        curr_event = log.event( i );

        %find sound file name and offset for event start time
        begin_file = get_current_file( fixed_sound, curr_event.time( 1 ) );
        start_offset =  curr_event.time( 1 ) - get_file_times( fixed_sound, begin_file );        

        %find path of sound files
        begin_file_full = fullfile( log.sound.path, begin_file );
% %         end_file_full   = fullfile( log.sound.path, end_file );
        
        %---
        % Add fields having to do with real date and time
        %---

        %calculate MATLAB datenum for event start and event stop
        real_start =  log.sound.realtime + map_time( log.sound, 'real', 'record', curr_event.time(1)) ./ 86400;
% %         real_stop  =  log.sound.realtime + map_time( log.sound, 'real', 'record', curr_event.time(2)) ./ 86400;

        %if sound files do not have readable time stamp, set real date and time to null and warn user
        if isempty(real_start)
            begin_date = '';
            begin_hour = '';
            xbat_date = '';
            xbat_time = '';
            
            % warn user if no warnings as of yet
            if ~flag
                
                % warn user for first sound file with no time stamp
                % (warning for every sound file would flood user!)
                if ~ismember(begin_file, badTimestampFiles)
                    badTimestampFiles = [badTimestampFiles, {begin_file}];
                    fail(sprintf('Time stamp in sound file name is unreadible:\n  %s' , begin_file ));
                    flag = true;
                end
            end
            
        %otherwise use add fields for real date and time
        else

            %calculate date and time strings for event start and stop time
            begin_date = datestr( real_start, 'mm/dd/yyyy' );
            begin_hour = datestr( real_start, 'HH' );

            %XBAT date and time values will not be automatically modified by Raven, so cannot be used as a check
            xbat_date = begin_date;
            xbat_time = datestr( real_start, 'HH:MM:SS.FFF' );
    % %         real_start_datetime = datestr( real_start, 'yyyymmdd_HHMMSS' );
    % %         real_stop_datetime  = datestr( real_stop,  'yyyymmdd_HHMMSS' );   

    % %         %find sound file name and offset for event stop time
    % %         currfile = find( strcmp( log.sound.file, begin_file ) );
    % %         sound_lens = log.sound.cumulative / log.sound.samplerate;
    % %         curr_sound_stop = sound_lens( currfile );     
    % % 
    % %         %if event spans multiple sound files
    % %         if curr_event.time( 2 ) > curr_sound_stop
    % % 
    % %            %if not last sound file
    % %             if currfile < length( log.sound.file )            
    % %                 stopfile = find( ( curr_event.time(2) < sound_lens ), 1, 'first' );
    % %                 end_file = log.sound.file{ stopfile };
    % %                 stop_offset =  curr_event.time( 2 ) - sound_lens( stopfile - 1 );
    % % 
    % %             %if last sound file, event must fall within file bounds
    % %             else
    % %                 end_file = log.sound.file{ currfile };
    % %                 stop_offset =  log.sound.samples( currfile ) / log.sound.samplerate;
    % %             end
    % % 
    % %         %event is contained within one file
    % %         else
    % %             end_file = begin_file;
    % % 
    % %             if isempty(curr_event.duration)
    % %                 curr_event.duration = curr_event.time( 2 ) - curr_event.time( 1 );
    % %             end
    % % 
    % %             stop_offset = start_offset + curr_event.duration;
    % %         end
        end

        %deal with multiple event tags
        tags = curr_event.tags{ 1 };
        for j = 2:length( curr_event.tags )
            tags = [tags ' | ' curr_event.tags{ j }];
        end

        %output line for current event to selection table
        line =  { i, ...
                  'Spectrogram 1', ...
                  curr_event.channel, ...
                  curr_event.time( 1 ), ...
                  curr_event.time( 2 ), ...
                  curr_event.freq( 1 ), ...
                  curr_event.freq( 2 ), ...
                  begin_date, ...
                  begin_hour, ...
                  begin_file_full, ...
                  begin_file, ...
                  start_offset, ...
                  xbat_date, ...
                  xbat_time, ...
                  curr_event.score, ...     
                  tags, ...
                  '', ...
                };
% %                   end_file_full, ...
% %                   stop_offset, ...
% %                   real_start_datetime, ...
% %                   real_stop_datetime, ...
        fprintf( fid, '%.0f\t%s\t%.0f\t%.17f\t%.17f\t%.17f\t%.17f\t%s\t%s\t%s\t%s\t%.17f\t%s\t%s\t%f\t%s\t%s\n', line{ : } );
%         fprintf( fid, '%.0f\t%s\t%.0f\t%.17f\t%.17f\t%.17f\t%.17f\t%s\t%.17f\t%s\t%.17f\t%s\t%s\t%.17f\t%s\n', line{ : } );
    end

    %--------------------------------------------------------------------------
    %close selection table
    %--------------------------------------------------------------------------
    fclose( fid );
