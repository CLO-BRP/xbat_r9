function fail(varargin)

% Displays text in warning dialog and in red font in MATLAB Desktop
%
% Input:
% ------
%	txt - string to display
%	dlg_name - title of warning dialog and text message to MATLAB Desktop

% History
%   msp2 - 27 Apr 2014
%       If dlg_name is not included in function call, set to "WARNING".
%   msp2 - 29 May 2014
%       Increase font size in warning dialog box.

txt = varargin{1};
if isequal(nargin,1)
    dlg_name = 'WARNING';
else
    dlg_name = varargin{2};
end

fprintf( 2,'\n\n%s\n%s\n', dlg_name, txt );

h = warndlg(txt, dlg_name);
pos = get( h, 'Position' );
set( h, 'Position', [ pos( 1 ), pos( 2 ), pos( 3 ) * 1.3, pos( 4 ) * 1.05 ] )
movegui( h, 'center' )

th = findobj( h, 'Tag', 'MessageBox' );
set( th, 'FontSize', 10, 'FontName', 'Segue UI' )
