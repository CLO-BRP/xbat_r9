function mg = browse_preset_menu( pal )

% browse_preset_menu - create Presets menu for Browse palette
% ------------------------------------------
%
% Input:
% ------
%  pal = handle for Browse palette
%
% Output:
% -------
%  mg = handles for Presets menu

% History
%   msp2 - 10 June 2014
%       When loading a preset, if a referenced log is not open, try to open it.
%   msp2 - 11 June 2014
%       Add button label to preset.  
%       Make buttons appear in proper order in preset.
%       Update presets list in Presets menu when saving presets.
%       Add callback to "Presets > Refresh Presets".
%       When saving and opening presets, unset duration and frequency is
%         set to [] or '' rather than NaN.
%   msp2 - 12 June 2014
%       Handle case where no logs are open in browser.
%       Set "Select Mode" checkbox to 1 after setting select mode in browser.
%   msp2 - 13 June 2014
%       Tweak contrasting font color for buttons.
%       Add line in preset for the position of each button (eg, "UPPER LEFT")
%   msp2 - 15 June 2014
%       Test that preset name is valid before saving.

L = { 'Presets' };
mg = menu_group( pal, 'browser_window_menu', L );
[ presets, names ] = get_palette_presets( 'Browse', 'csv' );
names = file_ext(names);
if (isempty(presets))
    L = { ...
        'Save Preset ...', ...
        'Clear Preset', ...
        'Show Presets' ...
    };
    S =  { ...
        'off', ...
        'off', ...
        'off' ...
    };
else
    L = { ...
        names{:}, ...
        'Save Preset ...', ...
        'Clear Preset', ...
        'Refresh Presets', ...
        'Show Presets' ...
    };
    n = length(L);
    S = bin2str(zeros(1,n));
    S{end - 3} = 'on';
end
mg2 = menu_group( mg, '', L, S );
set( mg2, 'callback' , { @preset_handler, pal } );

%% --------------------------------
function preset_handler( obj, ~, pal )

%--
% get menu label
%--
str = get(obj,'label');

%-------------------------------
% COMMAND SWITCH
%-------------------------------
switch (str)
	
	%-------------------------------
	% NEW PRESET
	%-------------------------------	
	case ('Save Preset ...')
		
		%--
		% create dialog controls
		%--		
		control(1) = control_create( ...
			'style','separator', ...
			'type','header', ...
			'min',1, ... 
			'string','Browse Preset', ...
			'space',0.75 ...
		);
		
		control(end + 1) = control_create( ...
			'name','name', ...
			'alias','Name', ...
			'style','edit', ...
			'value','New Preset' ...
		);
		
		%--
		% create dialog and get values
		%--		
		out = dialog_group('New Preset ...',control);		
        if (isempty(out.values))
			return;
        end
		fn = out.values.name;
        
        %--
        % Exit of valid filename not entered
        %--
        if ~proper_filename( fn )
            fail( sprintf( '"%s" is not a valid filename', fn ) );
            return;
        end
        
        %--
        % Open file for writing preset
        %--
        fpath = fullfile( xbat_root, 'Presets', 'Browse', [ fn '.csv' ] );
        if exist( fpath, 'file' )
            b = questdlg( sprintf( 'Preset "%s" already exists.  Overwrite?', fn ), 'WARNING' );
            if strcmp( b, 'Yes' )
                delete( fpath );
            else
                return;
            end
        end
        fid = fopen( fpath, 'a+' );
        
        %--
        % Write header identifying output in first line
        %--
        fprintf( fid, '%s\n\n', 'BROWSE PALETTE PRESET' );
        
        %--
        % Construct cell vector with button positions
        %--
        b_pos = { 
                  'BOTTOM RIGHT';
                  'BOTTOM LEFT';
                  'MIDDLE RIGHT';
                  'MIDDLE LEFT';
                  'TOP RIGHT';
                  'TOP LEFT';
        };
        
        %--
        % Construct cell matrix from button tooltips
        %--
        bh = findobj( pal, 'Type', 'uicontrol', 'Style', 'pushbutton' );
        
        for i = length( bh ) : -1 : 1
            
            %--
            % Get button label
            %--
            label = get( bh( i ), 'String' );
        
            %--
            % Get tooltips from buttons
            %--
            tooltip = get( bh( i ), 'TooltipString' );
            
            %--
            % Parse value fields into variables
            %--
            [ logname, mode_str, tag, duration, freq ] = parse_tooltip( tooltip );
            if ~isempty( freq )
                min_freq = freq( 1 );
                max_freq = freq( 2 );
            else
                min_freq = [];
                max_freq = [];
            end
            
            %--
            % Parse variables into comma-separated value pairs and write to text file
            %--
            fprintf( fid, [ ...
                            'POSITION:,%s\n' ...
                            'Label:,%s\n' ...
                            'Log:,%s\n' ...
                            'Mode:,%s\n' ...
                            'Tag:,%s\n' ...
                            'Duration( s ):,%.3f\n' ...
                            'Min Freq (Hz):,%.5f\n' ...
                            'Max Freq (Hz):,%.5f\n\n' ...
                          ], ...
                b_pos{ i }, label, logname, mode_str, tag, duration, min_freq, max_freq );
        end
        
        %--
        % Close preset file
        %--
        fclose( fid );
        
        %--
        % Refresh Presets menu
        %--
        refresh_presets( pal );
        
    %--
    % Refresh Presets menu
    %--
	case ('Refresh Presets')
        refresh_presets( pal )
        
	%-------------------------------
	% CLEAR PRESET
	%-------------------------------	
	case ('Clear Preset')
        
        % clear button label
        bh = findobj( pal, 'Type', 'uicontrol', 'Style', 'pushbutton' );        
        set( bh, 'String', '' )
        
        % reinitialize button color
        c = get( 0, 'DefaultUicontrolBackgroundColor' );        
        set( bh, 'String', '', 'BackgroundColor', c )
        
        % reinitialize button font
        set( bh, 'ForegroundColor', [ 0 0 0 ], 'FontSize', 12.5, 'FontWeight', 'normal' )
             
        % reinitialize button tooltips        
        tooltip = make_tooltip( '','','', [], [] );
        set( bh, 'TooltipString', tooltip )
        
        % delete mouse cursor icon on button
        set( bh, 'CData', [] )
        
        % set button state to unready
        set( bh, 'Userdata', struct( 'ready', 0 ) )
	
	%-------------------------------
	% SHOW PRESETS
	%-------------------------------	
	case ('Show Presets')
		
        % open Windows Explorer to Browse palette presets directory
		cmd = [ '!explorer /n,', fullfile( xbat_root, 'Presets', 'Browse' ), ' &' ];
		eval( cmd );
							
	%-------------------------------
	% LOAD SELECTED PRESET
	%-------------------------------	
    otherwise
        
        %--
        % Get names of active logs
        %--        
        par = get_active_browser;
        data = get( par, 'Userdata' );
        if ~isempty( data.browser.log )
            open_logs = log_name( data.browser.log );
        else
            open_logs = {''};
        end
        
        %--
        % Get library path
        %--        
        lib = get_active_library;
        snd = sound_name( data.browser.sound );
        lib_path = fullfile( lib.path, snd, 'Logs' );

        %--
        % load preset
        %--
        p = fullfile( xbat_root, 'Presets', 'Browse' );
        URI = fullfile( p, [ str '.csv' ] );
        state = txtread( URI, '%s%s', ',\t' );
        if ~isequal( size( state ), [ 49, 2 ] )
            fail( sprintf( '"%s" is not a proper preset', str ), 'WARNING' )
            return;
        else
            state = state( 2 : end, : );
        end
        
        %--
        % set parameters into buttons
        %--
        
        % find button handles
        bh = findobj( pal, 'Type', 'uicontrol', 'Style', 'pushbutton' );
        
        % reverse order of button handles
        bh = flipud( bh );
        
        % for each of 6 buttons
        for i = 1 : 6
            
            %--
            % get parameters from text
            %--
            start = ( i - 1 ) * 8 + 1;
            stop = i * 8;
            [ ~, label, logname, mode_str, tag, dur_str, min_freq_str, max_freq_str ] = deal( state{ start : stop, 2 } );
            
            %--
            % If log open, set ready to 1
            %--
            if isempty( logname )
                idx = 0;
            else
                idx = strcmp( open_logs, logname );
            end
            if any( idx )
                ready = 1;
                
            %--
            % If log not open, try to open.  Set ready to 0 or 1 depending on success.
            %--
            else
                
                % try to open log
                fn_full = fullfile( lib_path, [ logname, '.mat' ] );
                if exist( fn_full, 'file' )
                    flag = log_open( par, fn_full );
                    if flag
                        data = get( par, 'Userdata' );
                        if ~isempty( data.browser.log )
                            open_logs = log_name( data.browser.log );
                            idx = strcmp( open_logs, logname );
                        end
                    end
                end
                if any( idx )
                    ready = 1;
                else
                    ready = 0;
                    logname = '';
                end
            end
            
            %--
            % set tooltip into button
            %--
            dur = str2double( dur_str );
            min_freq = str2double( min_freq_str );
            max_freq = str2double( max_freq_str );
            tooltip = make_tooltip( logname, mode_str, tag, dur, [ min_freq, max_freq ] );
            set( bh( i ), 'Tooltipstring', tooltip )

            %--
            % set button color
            %--
            if ready
                set( bh( i ), 'BackgroundColor', data.browser.log( idx ).color )
            else
                set( bh( i ), 'BackgroundColor', get( 0, 'DefaultUicontrolBackgroundColor' ) ) 
            end
            
            %--
            % set button label   
            %--
            set( bh( i ), 'String', label )

            %--
            % set button font
            %--
            set( bh( i ), 'FontSize', 12.5, 'FontWeight', 'normal' )
            
            %--
            % set button font color to white, if background color dark
            %--
            c_back = get( bh( i ), 'BackgroundColor' );
            test = ( c_back( 1 ) < 0.75 ) && ( c_back( 2 ) < 0.97 );
            c_fore = test * ones( 1, 3 );
            set( bh( i ), 'ForegroundColor', c_fore )
        
            %--
            % if button ready, set bitmap for mode-specific cursor image in button
            %--
            tmp = [];
            
            %--
            % If a mode is preset for button
            %--
            if ~isempty( mode_str )
            
                % create cursor bitmap
                switch mode_str
                    case 'Select + Log'
                        tmp = ptr_add;
                    case 'One Click';
                        tmp = ptr_ibeam;
                    case 'One Click Frequency Tracking'
                        tmp = ptr_cross;
                    case 'Tag'
                        tmp = ptrT;
                end

                %convert bitmap to Truecolor
                tmp( tmp == 1 ) = 0;
                tmp( tmp == 2 ) = 1;
                [ m, n ] = size( tmp );
                tmp = [ NaN( m, 88 - n ), tmp ];  % position on right side of button
                cdata = repmat( tmp, [ 1, 1, 3 ] );
                set( bh( i ), 'CData', cdata )
                
            %--
            % If a mode is not preset for button
            %--
            else
                
                % the button is not ready
                ready = 0;
                
                % delete mouse cursor on button, if any
                set( bh( i ), 'CData', [] )
                
            end

            %--
            % set cursor into button
            %--
            set( bh( i ), 'Userdata', struct( 'ready', ready ) )
        end
        
        % set browser mode to "Select"
        set_browser_mode( par, 'select' )
        
        % Set Select Mode checkbox to 1
        cb = findobj( pal, 'Type', 'uicontrol', 'Style', 'checkbox' );
        set( cb, 'Value', 0 )
        select_mode = findobj( pal, 'Tag', 'select_mode' );
        set( select_mode, 'Value', 1 )
        
%         %--
%         % Put asterix in name of selected preset
%         %--
%         [ ~, names ] = get_palette_presets( 'Browse', 'csv' );
%         idx = strcmp( names, fn );
%         if ~isempty( idx )
%             names{ idx } = [ '*' names{ idx } ];
%         end
        
end

function refresh_presets( pal )
        
    % delete children of Presets menu
    mg = findobj( pal, 'Type', 'uimenu', 'Label', 'Presets' );
    m_child = get( mg, 'child' );
    delete( m_child )

    % get updated list of preset names
    [ presets, names ] = get_palette_presets( 'Browse', 'csv' );
    names = file_ext(names);

    % reconstruct Presets menu with new preset names
    if (isempty(presets))
        L = { ...
            'Save Preset ...', ...
            'Clear Preset', ...
            'Show Presets' ...
        };
        S =  { ...
            'off', ...
            'off', ...
            'off' ...
        };
    else
        L = { ...
            names{:}, ...
            'Save Preset ...', ...
            'Clear Preset', ...
            'Refresh Presets', ...
            'Show Presets' ...
        };
        n = length(L);
        S = bin2str(zeros(1,n));
        S{end - 3} = 'on';
    end
    mg2 = menu_group( mg, '', L, S );

    % add callbacks to Presets menu
    set( mg2, 'callback' , { @preset_handler, pal } );

%%
