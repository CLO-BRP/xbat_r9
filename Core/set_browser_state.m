function state = set_browser_state(par, state, opt)

% set_browser_state - set browser display state
% ---------------------------------------------
%
% state = set_browser_state(par, state, opt)
%
%   opt = set_browser_state
% 
% Input:
% ------
%  par - browser figure handle
%  state - browser display state
%  opt - state loading options
%
% Output: 
% -------
%  state - browser display state
%  opt - state loading options

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% History
%   msp2 - 31 October 2013
%       Allow browser to be positioned on non-primary monitor, or even off monitor
%       Insure browser opens on screen
%	msp2 - 1 January 2014
%		Reinitialize browser to "select" mode.
%   msp2 - 6 June 2014
%       When opening browser, reposition only if it is about to be positioned with the
%         grab bar off screen.
%   msp2 - 12 June 2014
%       Browse palette
%           Revise call to update_browse_palette to accommodate input parameter.
%           Update selection clipboard controls using selection clipboard in browser.
%           Disable setting browser sounds state into environment.
%       Allows sound detector palettes to position correctly by pausing at
%         the right place.
%       Disable caching of palette sounds state in Windows environment.

%--
% set default options
%--

if (nargin < 3) || isempty(opt)
	
	%--
	% create options structure
	%--
	
	opt.position = 1;
	
	opt.palettes = 1;
	
	opt.log = 1;
	
	opt.selection = 1;
	
	%--
	% output options structure
	%--
	
	if ~nargin
		state = opt; return;
	end
	
end
	
% %--
% % turn off sounds while we are doing this
% %--
% 
% palette_sounds = get_env('palette_sounds');
% 
% % NOTE: set the default value if not available
% 
% if isempty(palette_sounds)
% 	palette_sounds = 'on';
% end
% 
% set_env('palette_sounds', 'off');

%--
% turn off palette daemon if needed
%--

% NOTE: this can be encapsulated

timer_stop('XBAT Palette Daemon');

%-------------------------------------------------------------------------
% reposition browser if it is too far offscreen
%-------------------------------------------------------------------------

%get monitor positions of current system
temp = get( 0, 'MonitorPositions' );

%transform monitor position into the format MATLAB uses
screen_pos = ...
  [ temp( :, 1 ), ...
    temp( :, 2 ), ...
    temp( :, 3 ) - temp( :, 1 ) + 1, ...
    temp( :, 4 ) - temp( :, 2 ) + 1 ];

% find browser position
% (Note: browser is 40 pixels higher than MATLAB reports, due to title bar and menu bar)
pos = [ state.position( 1 : 3 ), state.position( 4 ) + 40 ];

% test separately for each monitor
num_monitors = size( screen_pos, 1 );
flag = false( 1, num_monitors );
for i = 1 : num_monitors
    
    %---
    % Test if browser extends at least 40 pixels into monitor on left,
    % right and bottom, and if the browser goes off the top even 1 pixel.
    %---
    top_flag =  pos( 2 ) + pos( 4 ) >= screen_pos( i, 2 ) + screen_pos( i, 4 );
    bottom_flag = pos( 2 ) + pos( 4 ) < screen_pos( i, 2 ) + 40;
    left_flag = pos( 1 ) + pos( 3 ) < screen_pos( i, 1 ) + 40;
    right_flag = pos( 1 ) > screen_pos( i, 1 ) + screen_pos( i , 3 ) - 40;
    flag( i ) = top_flag || bottom_flag || left_flag || right_flag;
end

%if browser is not entirely on screen, reset position in state
if all( flag )
    state.position = [  screen_pos( 1, 1 ) + 3,             ...
                        screen_pos( 1, 4 ) * 0.5,       ...
                        screen_pos( 1, 3 ) - 6,             ...
                        screen_pos( 1, 4 ) * 0.5 - 200  ];
end

%----------------------------------
% SET BROWSER POSITION
%----------------------------------

if opt.position
	
	set(par, 'position', state.position);

end

%----------------------------------
% OPEN PALETTES
%----------------------------------

if opt.palettes && ~isempty(state.palette)

	for k = 1:length(state.palette)

		try

			pal = [];
			
			%--
			% try to open palette using tag if available
			%--
			
			if isfield(state.palette(k), 'tag')
				
				info = parse_tag(state.palette(k).tag, '::', {'ignore', 'type', 'name'});
				
				info.type = lower(info.type);
				
				switch info.type
					
					case 'core'
						pal = browser_palettes(par, state.palette(k).name);
						
					otherwise
						pal = extension_palettes(par, info.name, info.type);
						
				end
				
			end
				
			%--
			% try to open palette by beating with a stick
			%--
			
			if isempty(pal)

				pal = browser_palettes(par, state.palette(k).name);

				% NOTE: in this case the function tries to infer the type

				if isempty(pal)
					pal = extension_palettes(par, state.palette(k).name);
				end
			
			end
			
			%--
			% set palette state
			%--
			
			% TODO: this step fails for detector palettes
			
			if ~isempty(pal)
                
                %--
                % pausing for browser palettes helps them position correctly
                %--
                if strcmp( info.type, 'sound_detector' )
                    pause( 0.1 )
                end
				set_palette_state(pal, state.palette(k));
			end

		catch

			nice_catch(lasterror, ['WARNING: Failed to open ''', state.palette(k).name, ''' palette.']);

        end

	end

end

%----------------------------------
% RESTORE PALETTE STATES
%----------------------------------

if opt.palettes && ~isempty(state.palette_states)

	data = get(par, 'userdata');

	data.browser.palette_states = state.palette_states;

	set(par, 'userdata', data);

end
	
%----------------------------------
% SET OPEN LOGS STATE
%----------------------------------

if opt.log && isfield(state, 'log') && ~isempty(state.log)

	%--
	% get library content from figure tag
	%--
	
	info = parse_tag(get(par, 'tag')); 
	
	lib = get_libraries([], 'name', get_library_file_name(info.library));
	
	%--
	% construct library log locations
	%--
	
	logs = strcat(lib.path, info.sound, [filesep, 'Logs', filesep], state.log.names, '.mat');
	
	%--
	% separate active log from other logs in list
	%--
	
	active = logs{state.log.active};
	
	logs(state.log.active) = [];
	
	%--
	% try to open logs from state
	%--
	
	% NOTE: we open active log last, and log open is given a no warning flag

	for k = 1:length(logs)
		flag = log_open(par, logs{k}, 0);
	end
	
	flag = log_open(par, active, 0);
      
    delete_flag = true;    %needs to be true because logs JUST OPENED!
    update_browse_palette( par, [], delete_flag );
	
	% TODO: consider notifying user if some log failed to load
	
end

%----------------------------------
% RESTORE SELECTION STATES
%----------------------------------

if opt.selection && isfield(state, 'selection') && ~isempty(state.selection)

	%--
	% update browser selection state
	%--
	
	% TODO: browsers should become objects
	
	data = get(par, 'userdata');

	data.browser.selection = state.selection;

	data.browser.selection.mode = 'select';

	set(par, 'userdata', data);
    
    %--
    % update Browse palette, if open
    %--
    delete_flag = true;    %needs to be true because logs JUST OPENED!
    update_browse_palette( par, [], delete_flag );

	%--
	% make sure that controls are in sync with state
	%--
	
	% NOTE: we recreate the palette so that current selection values will be loaded
	
	pal = get_palette(par, 'Selection', data);
	
	if ~isempty(pal)
		
		close(pal); browser_window_menu(par, 'Selection'); % create and set state
	
	end	
	
	%--
	% update selection display
	%--
	
	selection_update(par, data);
	
end

%--
% set palette sounds to previous state
%--

% NOTE: this is another problem related to failure to load state

% set_env('palette_sounds', palette_sounds);

%--
% restart daemon if needed
%--

timer_run('XBAT Palette Daemon');

	
