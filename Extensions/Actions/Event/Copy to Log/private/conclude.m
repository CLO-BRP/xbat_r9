function [result, context] = conclude(parameter, context)

% Copy To Log - conclude

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

%--
% save the file after events have been appended and update display
%--

result = struct;

%--
% get our bearings
%--

try
    par = context.callback.par.handle;
catch
    par = get_active_browser;
end

%--
% find log index
%--

names = log_name(get_browser(par, 'log'));
    
ix = find(strcmp(names, parameter.log{1}));

%--
% find log data structure
%--

data = get_browser(par); 
    
log = data.browser.log(ix);

%--
% save the log
%--

log_save(log);

%--
% update browser
%--

browser_display(par, 'events');

