function measurement = make_empty_measurement( measurement_function, parameter )
%make empty measurment struct with correct parameters

%create empty measurement struct with default parameters
measurement = measurement_function( 'create' );

%strip parameter fields not relating to selected event measurement
all_fields = fieldnames( parameter );
curr_fields = fieldnames( measurement.parameter );
parameter_cleaned = rmfield( parameter, all_fields( ~ismember( all_fields, curr_fields ) ) );

measurement.parameter = parameter_cleaned;
measurement.author = parameter.author;

%convert cell string to string for parameters controlled by popup controls
if strcmp( parameter.measurements{ 1 }, 'CSE Location (v2.3)' )

  measurement.parameter.corr_type = measurement.parameter.corr_type{ 1 };
  measurement.parameter.channels = str2double( measurement.parameter.channels )';
  measurement.parameter.search_dim = measurement.parameter.search_dim{ 1 };
  measurement.parameter.display_plots = measurement.parameter.display_plots{ 1 };
  measurement.parameter.spc_win_type = measurement.parameter.spc_win_type{ 1 };
  measurement.parameter.error_est = measurement.parameter.error_est{ 1 };
  measurement.parameter.test = measurement.parameter.test{ 1 };

else
  measurement.parameter.fftsz = str2double( measurement.parameter.fftsz{ 1 } );
  measurement.parameter.winfunc = measurement.parameter.winfunc{ 1 };
  measurement.parameter.use_stdFreq = measurement.parameter.use_stdFreq{ 1 };
end
