function data = set_filter( fh, i, parameter, context )
%set signal filter into browser, if specified by user
  
filter_name = parameter.filter_name{1};
preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Log', ...
                       context.ext.name, ...
                       'private', ...
                       'filter_struct');
                     
if strcmp( filter_name, 'None' )
  data = get( fh,'userdata' );
  set( fh, 'Visible', 'off' )

else

  %if first log
  if isequal( i, 1 )      
    %call appropriate signal filter palette
    filter_name = [ filter_name, ' ...' ];
    out = browser_filter_menu( fh, filter_name, 'signal_filter' );
    uiwait
    set( fh, 'Visible', 'off' )
    if isempty( out )
      d = sprintf( 'Problem applying "%s" filter to "%s".', filter_name, snd_name );
      fail( d, 'WARNING' )
      return;
    end

    %save signal filter struct
    data = get( fh,'userdata' );
    filter_struct = data.browser.signal_filter;
    save( preset_path, 'filter_struct' );

  %if not first log, set signal filter struct into browser and refresh display
  else
    load( preset_path );
    data = get( fh,'userdata' );
    data.browser.signal_filter = filter_struct;
    set( fh, 'Userdata', struct( 'browser', data.browser ) )
    browser_display(fh,'update',data);
    set( fh, 'Visible', 'off' )
  end
end