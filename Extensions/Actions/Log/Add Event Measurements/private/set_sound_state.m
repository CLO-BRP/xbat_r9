function [ snd_struct_path, orig_sound_struct ] = set_sound_state( snd_name, parameter, context )
%set sound state per parameter

%Set aside original sound and state structs
snd_struct_path = fullfile( context.library.path, snd_name, [snd_name '.mat' ] );
orig_sound_struct = load( snd_struct_path );

%modify sound or snd struct
f = fieldnames( orig_sound_struct );
snd_field = f( ~strcmp( 'state', f ) );

snd = orig_sound_struct.( snd_field{ 1 } );

% for CSE Location, set spectrogram parameters into sound
if strcmp( parameter.measurements{ 1 }, 'CSE Location (v2.3)' )
  channel_binary = zeros( max( snd.channels, length( parameter.channels ) ) ,1 );
  for j = 1:length(parameter.channels)
    channel = str2double( strrep( parameter.channels{ j }, 'Channel ', '' ) );
    channel_binary( channel ) = 1;
  end
  channel_binary = channel_binary(1:snd.channels);
  channel_list = (1:snd.channels)';
  snd.view.channels = [channel_list , channel_binary];

  snd.view.time = 0;
  snd.view.page.duration = 1;
%   snd.view.page.duration = 60000 / snd.samplerate;

  snd.specgram.fft = parameter.spc_fft;
  snd.specgram.hop = parameter.spc_hop;
  snd.specgram.hop_auto = 0;
  snd.specgram.win_type = parameter.spc_win_type{1};
  snd.specgram.win_param = [];
  snd.specgram.win_length = parameter.spc_win_length;

  snd.view.page.overlap = 0;
  snd.view.page.freq = [];
  snd.view.grid.on = 1;
  snd.view.grid.color = [ 0.25 0.25 0.25 ];
  snd.view.grid.time.on = 0;
  snd.view.grid.time.spacing = 1;
  snd.view.grid.time.labels = 'clock';
  snd.view.grid.time.realtime = 1;
  snd.view.grid.file.on = 1;
  snd.view.grid.file.labels = 1;
  snd.view.grid.session.on = 1;
  snd.view.grid.session.labels = 1;
  snd.view.grid.freq.on = 0;
  snd.view.grid.freq.spacing = [];
  snd.view.grid.freq.labels = 'kHz';
  snd.view.colormap.name = 'Gray ';
  snd.view.colormap.invert = 1;
  snd.view.colormap.auto_scale = 1;
  snd.view.colormap.brightness = 0.5;
  snd.view.colormap.contrast = 0;

  snd.specgram.on = 1;
  snd.specgram.diff = 0;
  snd.specgram.filter = 'None';
  snd.modified = now;
end

%create standard sound state, with palettes closed
MonitorPosition = get( 0, 'MonitorPosition' );
state.screensize = MonitorPosition( 1, : );
state.position = state.screensize;  
state.palette = [];
state.palette_states = [];
state.palette_states.name = 'Spectrogram';
state.palette_states.tag = 'XBAT_PALETTE::CORE::Spectrogram';
state.palette_states.position = [954 87 200 595];
state.palette_states.toggle = [];
state.palette_states.tabs.tab = 'FFT';
state.log = [];
state.selection.on = 1;
state.selection.mode = 2;
state.selection.zoom = 0;
state.selection.event = struct( zeros( 1, 0 ) );
state.selection.log = [];
state.selection.handle = [];
state.selection.color = [ 1 0 0.05 ];
state.selection.linestyle = '-';
state.selection.linewidth = 1;
state.selection.patch = 0;
state.selection.grid = 1;
state.selection.label = 0;
state.selection.control = 1;
state.selection.copy = [];

%save modified sound/snd and state structs
save( snd_struct_path, 'snd', 'state' );