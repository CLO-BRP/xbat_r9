function [result, context] = prepare(parameter, context)
% COUNT TAGS - prepare
% Makes a list of all tags used in selected log(s) in MATLAB Command Window
%
% Michael Pitzrick
% Cornell Lab of Ornithology
% msp2@cornell.edu

%History
%   msp2 - 14 Mar 2014
%       Fix bug active when log with no events is processed.

%initializations
result = struct;
tstart = tic;
tags = {};
count = [];
log_names = {};
% k = 0;

%Stop if any open browsers
if close_browsers
    return;
end

%retrieve list of selected logs
logs = get_logs( context );
if isempty( logs )
    fail( 'API supplied no context.target in prepare.m.', 'WARNING' );
    return;
end
NumLogs = length( logs );

%waitbar
waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
wh = findobj( 'tag', waitbar_name );

for i = 1:NumLogs
    
    t0 = tic;

    %indicate log being processed in waitbar
    waitbar_update( wh(1), 'PROGRESS', 'message', ['Processing ', logs{ i }, ' ...'] );

    %load log, refresh file name and path
    [ log, ~, status ] = load_log( logs{ i }, context );
    if status
        return;
    end
    
    %----------------------------------------------------------------------
    %make list of event tags in log
    %----------------------------------------------------------------------
    tags_flat = {};
    log_names = [ log_names { log_name( log ) } ];
    if log.length
        e = log.event;
%         k = k + 1;

        for j = 1:log.length
            curr_tags = get_tags( e( j ) );
            if isempty( curr_tags )
                curr_tags = { '(no tag)' };
            end
            tags_flat = [ tags_flat; curr_tags ];
            if length( curr_tags ) > 1
                d = sprintf( 'Multiple tags in event #%.0f in "%s"', ...
                    log.event( j ).id, log_name( log ) );
                fail( d, 'WARNING' )
            end
        end

        %remove asterisk from beginning of tag
        for j = 1:length( tags_flat )
            curr_tag = tags_flat{ j };
            if strcmp( curr_tag( 1 ), '*' )
                curr_tag = curr_tag( 2:end );
                tags_flat{ j } = curr_tag;
            end
        end

        %----------------------------------------------------------------------
        %make vector of unique tags, and vector of count for each
        %----------------------------------------------------------------------
        curr_tags_unique = unique( tags_flat );
        num_tags = length( curr_tags_unique );
        curr_count = zeros( num_tags, 1 );
        for j = 1:num_tags
            curr_count( j ) = sum( strcmp( curr_tags_unique{ j }, tags_flat ) );
        end
    else
       fail( sprintf( 'No events in "%s"', log_name( log ) ), 'WARNING' );
       curr_count = zeros( num_tags, 1 );
    end
    
    tags = [ tags, { curr_tags_unique } ];
    count = [ count, { curr_count } ];

    %display log processed in waitbar
    result.target = log;
    result.status = 'done';
    result.message = '';
    result.error = [];
    result.elapsed = toc( t0 );
    result.output = struct( [] );
    action_waitbar_update( wh(1), i/NumLogs, result, 'log' ); 
    
end

%----------------------------------------------------------------------
%find master list of unique tags
%----------------------------------------------------------------------
tags_flat = {};
for i = 1:length( tags )
    tags_flat = [ tags_flat; tags{ i } ];
end
tag_list = unique( tags_flat );

%----------------------------------------------------------------------
%make tag count matrix for master tag list by log
%----------------------------------------------------------------------
num_tags = length( tag_list );
num_logs = length( count );
count2 = zeros( num_logs, num_tags );
for j = 1:num_tags
    curr_tag = tag_list{ j };
    for i = 1:num_logs
        tag_match = strcmp( curr_tag, tags{ i } );
        if any( tag_match )
            count2( i, j ) = count{ i }( tag_match );
        end
    end
end

%----------------------------------------------------------------------
%output to MATLAB Desktop
%----------------------------------------------------------------------
fprintf( '\n\n' );
fprintf( '%10s', tag_list{ : } );
fprintf( '\n' );
for i = 1:num_logs
    fprintf( '%10.0f', count2( i, : ) );
    eval( 'fprintf( ''   %s'', log_names{ i } );' );
    fprintf( '\n' );
end
fprintf( '\n' );
fprintf( '%10.0f', sum( count2 ) );
fprintf( '   SUM\n' );

%----------------------------------------------------------------------
%output to XLSX, if enabled by user
%----------------------------------------------------------------------
if parameter.xlsx
    
    %find output path
    xlsx_name = sprintf( 'Count_tag_%s_%s.xlsx', ...
        context.library.name, datestr( now, 'yyyymmdd_HHMMSSFFF' ) );
    xlsx_path = fullfile( parameter.out_dir, xlsx_name );
    
    %header
    M( 1, 1 ) = { 'Count Tags in Logs' };
    M( 2, 1:3 ) = { 'Run time:', '', datestr( now ) };
    
    %column heads
    M( 4, 1 ) = { 'Log Names' };
    num_tags = length( tag_list );
    M( 4, 2:num_tags + 1 ) = tag_list;
    j = 4;
    k = num_tags + 1;
    count_cell = num2cell( count2 );
    
    %data
    for i = 1:num_logs
        M( j + i, 1:k ) = [ log_names( i ), count_cell( i, : ) ];
    end

    %write xlsx
    xlswrite( xlsx_path, M );
end

%indicate action finished in waitbar
d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc(tstart) );
waitbar_update( wh(1), 'PROGRESS', 'message', d );

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error( 'No Error' )  %To avoid "Prepare failed" dialog, us new action_dispatch.m