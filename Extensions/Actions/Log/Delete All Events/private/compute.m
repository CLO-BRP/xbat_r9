function [result, context] = compute(log, parameter, context)
result = struct;

% DELETE ALL EVENTS - compute

if context.state.kill
  return;
end

% check if log is open and there are deleted events to clear

if log_is_open(log)
    disp(['Close log ''', log_name(log), ' before deleting events.']); 
    return;
end

%trim extension off log name
fn = log.file;
fn = fn(1:end-4);

%terminate script if no events to delete
if log.length < 1
  fprintf(2,'No events in %s\n', fn);
  return;
end

%copy all events to deleted_event
log.deleted_event = [log.deleted_event log.event];

%overwrite events with empty event
log.event = empty(event_create);

%set log length to zero
log.length = 0;

%save log
log_save(log);
