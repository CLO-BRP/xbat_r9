function [result, context] = compute(log, parameter, context)

% DELETE TAGS - compute

result = struct;

%don't run if no events
if context.state.kill || log.length == 0
  return;
end

%delete tags
for i = 1:log.length
  log.event(i).tags = cell(0);
end

log_save(log);
