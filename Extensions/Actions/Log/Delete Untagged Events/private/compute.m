function [result, context] = compute(log, parameter, context)

%% DELETE UNTAGGED EVENTS (log action) - compute
%
% Deletes untagged events
%
%Mike Pitrick
%msp2@cornell.edu
%2 Aug 08
%
%%
result = struct;

% check if log is open and there are deleted events to clear
if log_is_open(log)
    disp(['Close log ''', log_name(log), ' before running this action.']); return;
end

% determine log name
fnLog = log.file;
lenfnLog = length(fnLog);
fnLog = fnLog(1:lenfnLog-4);

%sort into events with tags and events without tags
TaggedEvent = empty(event_create);

NumTaggedEvents = 0;
NumUntaggedEvents = 0;

for i = 1:length(log.event)
%  if ~(isequal(log.event(i).tags,{}))  %no tag for event
  if ~isequal(length(log.event(i).tags),0)
    NumTaggedEvents = NumTaggedEvents + 1;
    TaggedEvent(NumTaggedEvents) = log.event(i);
  else                                  %event has 1 or more tags
    NumUntaggedEvents = NumUntaggedEvents + 1;
    UntaggedEvent(NumUntaggedEvents) = log.event(i);
  end
end

%stop if no Untagged Events
if isequal(NumUntaggedEvents,0)
  disp(sprintf('\n\n%s %s\n','There are no untagged events in log', fnLog))
  
else
  %create new empty log
  NewLogName = [fnLog '_TaggedEventsOnly'];
  NewLog = new_log(NewLogName,context.user,context.library,context.sound);

  %keep only tagged events
  NewLog.event = TaggedEvent;
  NewLog.length = NumTaggedEvents;

  %delete untagged events
  OldDeletedEvents = NewLog.deleted_event;
  NewLog.deleted_event = [OldDeletedEvents UntaggedEvent];

  %save new log
  log_save(NewLog);
end

%event_create