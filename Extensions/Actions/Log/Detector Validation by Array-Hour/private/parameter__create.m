function parameter = parameter__create(context)

% DETECTOR EVAL BY ARRAY-HOUR - parameter__create

parameter = struct;

preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Log', ...
                       context.ext.name, ...
                       'private', ...
                       'preset.mat');

try
% if exist(preset_path,'file')
    
    load(preset_path)
    		
		%--
		%test if parameter fields have changed
		%--
		
		field = {'truth_str'
			       'pos_truth_tag'
			       'neg_truth_tag'
			       'test_str'
			       'pos_test_tag'
			       'pvalue'
			       'out_dir'};
					 
		assert( all( isfield( parameter, field )));
    
catch
% else
										 
	parameter.truth_str = 'h';

	parameter.pos_truth_tag = 's';

	parameter.neg_truth_tag = 'n';

	parameter.test_str = 'CRA';

	parameter.pos_test_tag = 'sprmclk_prt';
	
	parameter.pvalue = 0.95;

	parameter.out_dir = pwd;

	save(preset_path,'parameter');
	
end
