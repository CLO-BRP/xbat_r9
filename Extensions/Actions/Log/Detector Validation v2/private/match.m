function [count, ann_truth_log, ann_test_log] = match(truth_log, test_log, parameter)


% positive truth tags in truth log?
if isempty(parameter.pos_truth_tag)

  pos_tag_filter_flag = 0;
  
else
  
  pos_tag_filter_flag = 1;
  
end

%% make truth log matrix
truth_M = make_log_matrix(truth_log, 'truth');

truth_time = truth_M(:,1:2);

truth_chan = truth_M(:,5);

truth_tags = get_primary_tag(truth_log.event, truth_log.length);

if pos_tag_filter_flag

  pos_truth = strcmpi(parameter.pos_truth_tag, truth_tags);
  
else

  pos_truth = true(truth_log.length,1);  
  
end

pos_truth_size = sum(pos_truth);

if parameter.truth_flag

  neg_truth = strcmpi(parameter.neg_truth_tag, truth_tags);
  
  neg_truth_size = sum(neg_truth);
  
% else
% 
%   neg_truth = false(truth_log.length,1);  
  
end


%% make test log matrix
test_M = make_log_matrix(test_log, 'test');

%adjust begin and end time of test events by event duration * overlap
fudge = parameter.overlap * diff(test_M(:,1:2)')';

test_time_fudge = [test_M(:,1) + fudge, test_M(:,2) - fudge];

test_chan = test_M(:,5);
  
test_score = test_M(:,6)';


%% classify truth events as TP, FN, FP, and TN

match = false(truth_log.length,1);

% truth_out_tag = cell(truth_log.length,1);

truth_score = NaN(1,truth_log.length);

% match_idx = NaN(1,truth_log.length);


for i = 1:truth_log.length
  
  match_vector = (truth_time(i,1) <  test_time_fudge(:,2)) ...
               & (truth_time(i,2) >= test_time_fudge(:,1)) ...
               & (truth_chan(i)   ==       test_chan(:));
             
  curr_match = any(match_vector);
             
  match(i) = curr_match;
  
%   match(i) = any((truth_time(i,1) <  test_time_fudge(:,2)) ...
%                & (truth_time(i,2) >= test_time_fudge(:,1)) ...
%                & (truth_chan(i)   ==       test_chan(:)) ...
%              );

  if parameter.roc_flag && curr_match
    
    %find max score of test events matching current truth event
%     test_score_match = test_score(match_vector);
%     
%     curr_match_idx = find(match_vector);

%     [truth_score(i), max_idx] = max(test_score_match);
    truth_score(i) = max(test_score(match_vector));

%     match_idx(i) = curr_match_idx(max_idx);
    
  end
           
  
%   %if truth event is matched by test event
%   if any((truth_time(i,1) <  test_time_fudge(:,2)) ...
%        & (truth_time(i,2) >= test_time_fudge(:,1)) ...
%        & (truth_chan(i)   ==       test_chan(:)) ...
%      )
%   
%      if pos_truth(i)
%        
%        truth_out_tag{i} = 'TP';
%        
%      elseif neg_truth(i)
%        
%        truth_out_tag{i} = 'FP';
%        
%      end
%   
%   %if truth event is not matched by test event
%   else
%   
%      if pos_truth(i)
%        
%        truth_out_tag{i} = 'FN';
%        
%      elseif neg_truth(i)
%        
%        truth_out_tag{i} = 'TN';
%        
%      end
%   end
    
end

% match_idx = ~isnan(match_idx);
% 
% match_idx = unique(match_idx);


%% annotate truth log

ann_truth_log = truth_log;

ann_truth_log.event = set_tags(ann_truth_log.event, '');

ann_truth_log.event(logical(match & pos_truth(:))) = set_tags(ann_truth_log.event(logical(match(:) & pos_truth)), 'TP');

ann_truth_log.event(logical(~match & pos_truth(:))) = set_tags(ann_truth_log.event(logical(~match(:) & pos_truth)), 'FN');

if parameter.truth_flag

  ann_truth_log.event(logical(match & neg_truth(:))) = set_tags(ann_truth_log.event(logical(match(:) & neg_truth)), 'FP');

  ann_truth_log.event(logical(~match & neg_truth(:))) = set_tags(ann_truth_log.event(logical(~match(:) & neg_truth)), 'TN');
  
end

fn = log_name(truth_log);

fn = fn(1:min(namelengthmax-13, length(fn)));

fn = [fn, '_AnnTruth'];

ann_truth_log.file = fn;


%% annotate test log

ann_test_log = test_log;

for i = 1:test_log.length
  
  %if test event is matched by a truth event
  if any(pos_truth(:) ...
       & truth_chan(:)   == test_chan(i) ...
       & truth_time(:,1) <  test_time_fudge(i,2) ...
       & truth_time(:,2) >= test_time_fudge(i,1) ...
       & truth_chan(:)   == test_chan(i))
      
    ann_test_log.event(i) = set_tags(ann_test_log.event(i), 'TP');
    
  elseif ~parameter.truth_flag ...                   %no negative truth events
         || any(neg_truth(:) ...                    %matches negative truth event
           & truth_chan(:)   == test_chan(i) ...
           & truth_time(:,1) <  test_time_fudge(i,2) ...
           & truth_time(:,2) >= test_time_fudge(i,1) ...
           & truth_chan(:)   == test_chan(i))
      
    ann_test_log.event(i) = set_tags(ann_test_log.event(i), 'FP');
    
  end  
end

fn = log_name(test_log);

fn = fn(1:min(namelengthmax-13, length(fn)));

fn = [fn, '_AnnTest'];

ann_test_log.file = fn;


%% calculate counts for truth log

if parameter.roc_flag

  truth_tags = get_tags(ann_truth_log.event);
  

  TP_truth = strcmp('TP',[truth_tags{:}]);

  FP_truth = strcmp('FP',[truth_tags{:}]);

  FN_truth = strcmp('FN',[truth_tags{:}]);

  TN_truth = strcmp('TN',[truth_tags{:}]);
  
  
  TP_truth_count = zeros(20, 1);
    
  FP_truth_count = TP_truth_count;
    
  FN_truth_count = TP_truth_count;
    
  TN_truth_count = TP_truth_count;
  
  
  test_tags = get_tags(ann_test_log.event);
  

  TP_test = strcmp('TP',[test_tags{:}]);

  FP_test = strcmp('FP',[test_tags{:}]);
  
  
  TP_test_count = TP_truth_count;
  
  FP_test_count = TP_truth_count;
  
  
  test_score_used = test_score(~cellfun(@isempty, test_tags));
  
  threshold = 0 : 0.05 : 1;
  
  
  for i = 1:20
    
    curr_T = threshold(i);
    
    score_filter_truth = truth_score >= curr_T;
    
  
    TP_truth_count(i) = sum(TP_truth & score_filter_truth);

    FN_truth_count(i) = pos_truth_size - TP_truth_count(i);
%     FN_truth_count(i) = sum(FN_truth & score_filter_truth);

    if parameter.truth_flag

      FP_truth_count(i) = sum(FP_truth & score_filter_truth);

      TN_truth_count(i) = neg_truth_size - FP_truth_count(i);
%       TN_truth_count(i) = sum(TN_truth & score_filter_truth);
  
    end
    
    score_filter_test = test_score_used >= curr_T;
    
    TP_test_count(i) = sum(TP_test & score_filter_test);
    
    FP_test_count(i) = sum(FP_test & score_filter_test); 
    
  end

  
  count.truth.TP = TP_truth_count;

  count.truth.FP = FP_truth_count;

  count.truth.FN = FN_truth_count;

  count.truth.TN = TN_truth_count;

  count.truth.total = truth_log.length;


  count.test.TP = TP_test_count;

  count.test.FP = FP_test_count;

  count.test.total = test_log.length;
  

elseif parameter.xls_flag

  truth_tags = get_tags(ann_truth_log.event);

  count.truth.TP = sum(strcmp('TP',[truth_tags{:}]));

  count.truth.FP = sum(strcmp('FP',[truth_tags{:}]));

  count.truth.FN = sum(strcmp('FN',[truth_tags{:}]));

  count.truth.TN = sum(strcmp('TN',[truth_tags{:}]));

  count.truth.total = truth_log.length;


  %% calculate counts for test log

  test_tags = get_tags(ann_test_log.event);

  count.test.TP = sum(strcmp('TP',[test_tags{:}]));

  count.test.FP = sum(strcmp('FP',[test_tags{:}]));

  count.test.total = test_log.length;

end


