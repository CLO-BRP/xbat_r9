function parameter = parameter__create(context)

% DETECTOR VALIDATION - parameter__create

parameter = struct;

parameter.truth_suffix = 'SNR';

parameter.test_suffix = 'DET';

parameter.overlap = 0.5;

parameter.plevel = 0.9;

parameter.pos_truth_tag = '';

parameter.neg_truth_tag = '';

parameter.fig_flag = 1;

% parameter.xls_flag = 0;
parameter.xls_flag = 1;

parameter.roc_flag = 1;

% parameter.annot_truth_log_flag = 0;
parameter.annot_truth_log_flag = 1;

% parameter.annot_test_log_flag = 0;
parameter.annot_test_log_flag = 1;

parameter.sound_check_flag = 0;
