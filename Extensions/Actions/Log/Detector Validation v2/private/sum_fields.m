function cum_count = sum_fields(cum_count, count, parameter)

if parameter.roc_flag
  
  IDX = 1:20;
  
else
  
  IDX = 1;
  
end

for i = IDX

  cum_count.truth.TP(i) = cum_count.truth.TP(i) + count.truth.TP(i);

  cum_count.truth.FN(i) = cum_count.truth.FN(i) + count.truth.FN(i);

  if parameter.truth_flag

    cum_count.truth.FP(i) = cum_count.truth.FP(i) + count.truth.FP(i);

    cum_count.truth.TN(i) = cum_count.truth.TN(i) + count.truth.TN(i);

  end


  cum_count.test.TP(i) = cum_count.test.TP(i) + count.test.TP(i);

  cum_count.test.FP(i) = cum_count.test.FP(i) + count.test.FP(i);

end

cum_count.truth.total = cum_count.truth.total + count.truth.total;

cum_count.test.total = cum_count.test.total + count.test.total;

