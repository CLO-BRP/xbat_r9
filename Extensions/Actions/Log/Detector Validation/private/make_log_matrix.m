function M = make_log_matrix(log, log_type)

if strcmp(log_type, 'test')

  %Columns of M
  %  1. time(1)
  %  2. time(2)
  %  3. freq(1)
  %  4. freq(2)
  %  5. channel
  %  6. score

  M = zeros(log.length, 6);

  for i = 1:log.length

    M(i,1:2) = log.event(i).time;

    M(i,3:4) = log.event(i).freq;

    M(i,5)   = log.event(i).channel;

    curr_score = log.event(i).score;
    if isempty(curr_score)
        txt = sprintf('Action terminated.  Event # %.0f has no score in log named %s.', ...
              log.event(i).id, log_name(log));
          
        error(txt)
% %         fail(txt)
% %         fprintf(2, '\n\nWARNING:\n%s\n', txt);
        return;
    end
    M(i,6)   = curr_score;

  end

elseif strcmp(log_type, 'truth')

  %Columns of M
  %  1. time(1)
  %  2. time(2)
  %  3. freq(1)
  %  4. freq(2)
  %  5. channel

  M = zeros(log.length, 5);

  for i = 1:log.length

    M(i,1:2) = log.event(i).time;

    M(i,3:4) = log.event(i).freq;

    M(i,5)   = log.event(i).channel;

  end
  
else
  
  fprintf(2,'"make_log_matrix" input variable has illegal value for %s\n', log_name(log));
  
end
