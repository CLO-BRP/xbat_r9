function [count, annotated_truth_log, annotated_test_log] = match(truth_log, test_log, overlap)

% fields in count
%
%   count.truth.TP
%
%   count.truth.FN
%
%   count.test.TP
%
%   count.test.FP


% fields in match
%
%   match.truth - truth event matches test event (0/1)
%
%   match.test - test event matches truth event(0/1)


%% make truth log matrix
truth_M = make_log_matrix(truth_log, 'truth');

truth_time = truth_M(:,1:2);

truth_chan = truth_M(:,5);

%% make test log matrix
test_M = make_log_matrix(test_log, 'test');

%adjust begin and end time of test events by event duration * overlap
fudge = overlap * diff(test_M(:,1:2)')';

test_time_fudge = [test_M(:,1) + fudge, test_M(:,2) - fudge];

test_chan = test_M(:,5);

%% determine whether truth events are TP or FN (1 or 0)

match = struct('truth', zeros(truth_log.length,1), 'test', zeros(test_log.length,1));

for i = 1:truth_log.length
  
  match.truth(i) = any((truth_time(i,1) <  test_time_fudge(:,2)) ...
                     & (truth_time(i,2) >= test_time_fudge(:,1)) ...
                     & (truth_chan(i)   ==       test_chan(:)) ...
                   );
  
end

%determine whether test events are TP or FP (1 or 0)
for i = 1:test_log.length
  
  match.test(i) = any((truth_time(:,1) <  test_time_fudge(i,2)) ...
                    & (truth_time(:,2) >= test_time_fudge(i,1)) ...
                    & (truth_chan(:)   ==       test_chan(i)) ...
                   );
  
end


%% annotate truth log
annotated_truth_log = truth_log;

new_truth_event = set_tags(truth_log.event, 'FN');

new_truth_event(logical(match.truth(:))) = set_tags(truth_log.event(logical(match.truth(:))), 'TP');

annotated_truth_log.event = new_truth_event;

fn = log_name(truth_log);

fn = fn(1:min(namelengthmax-13, length(fn)));

fn = [fn, '_AnnTruth'];

annotated_truth_log.file = fn;


%% annotate test log
annotated_test_log = test_log;

new_test_event = set_tags(test_log.event, 'FP');

new_test_event(logical(match.test(:))) = set_tags(test_log.event(logical(match.test(:))), 'TP');

annotated_test_log.event = new_test_event;

fn = log_name(test_log);

fn = fn(1:min(namelengthmax-13, length(fn)));

fn = [fn, '_AnnTest'];

annotated_test_log.file = fn;


%% calculate counts

count.truth.TP = sum(match.truth);

count.truth.FN = length(match.truth) - count.truth.TP;

count.test.TP = sum(match.test);

count.test.FP = length(match.test) - count.test.TP;
