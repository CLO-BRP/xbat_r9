function M_new = out_matrix(M, count, duration, parameter, fn_truth, fn_test)

%if no negative truth events (TN not defined explicitly)
if ~parameter.truth_flag
  
  %if writing summary line
  if strcmp(fn_truth, 'TOTAL')
  
    [tpr, ci] = binofit(count.truth.TP, count.truth.TP + count.truth.FN, 1 - parameter.plevel);
    M_new{2,1} = tpr;
    M_new{2,2} = ci(1);
    M_new{2,3} = ci(2);

    M_new{2,7} = count.truth.TP;
    M_new{2,8} = count.truth.FN;
    M_new{2,9} = count.truth.TP + count.truth.FN;

    M_new{2,10} = count.test.TP;
    M_new{2,11} = count.test.FP;
    M_new{2,12} = count.test.TP + count.test.FP;

    M_new{2,13} = fn_truth;
    M_new{2,14} = fn_test;
  
    M_new{2,4} = count.truth.TP / (count.truth.TP + count.truth.FN);
    M_new{2,5} = count.test.FP / (duration / 3600);
    M_new{2,6} = count.test.TP / (count.test.TP + count.test.FP);

    M_new{2,13} = 'COMBINED LOGS';
    M_new{2,14} = ' ';
    
  %if not writing summary line
  else
  
    [tpr, ci] = binofit(count.truth.TP, count.truth.TP + count.truth.FN, 1 - parameter.plevel);
    M_new{1,1} = tpr;
    M_new{1,2} = ci(1);
    M_new{1,3} = ci(2);
    
    M_new{1,4} = count.truth.TP / (count.truth.TP + count.truth.FN);
    
    M_new{1,5} = count.test.FP / (duration / 3600);
    M_new{1,6} = count.test.TP / (count.test.TP + count.test.FP);

    M_new{1,7} = count.truth.TP;
    M_new{1,8} = count.truth.FN;
    M_new{1,9} = count.truth.TP + count.truth.FN;

    M_new{1,10} = count.test.TP;
    M_new{1,11} = count.test.FP;
    M_new{1,12} = count.test.TP + count.test.FP;

    M_new{1,13} = fn_truth;
    M_new{1,14} = fn_test;    
    
  end

  
%if negative truth events (TN defined explicitly)  
else
  
  %fill in later
  
end

M_new = [M; M_new];
