function cum_count = sum_fields(cum_count, count)

cum_count.truth.TP = cum_count.truth.TP + count.truth.TP;

cum_count.truth.FN = cum_count.truth.FN + count.truth.FN;

cum_count.test.TP = cum_count.test.TP + count.test.TP;

cum_count.test.FP = cum_count.test.FP + count.test.FP;
