function [result, context] = compute(log, parameter, context)

% Copyright (C) 2002-2014 Cornell University

%
% This file is part of XBAT.
% 
% XBAT is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% XBAT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with XBAT; if not, write to the Free Software
% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

% EMPTY TRASH - compute

result = struct;

%--
% stop execution if prepare.m failed
%--

if context.state.kill
	return;
end

%--
% check if log is open and there are deleted events to clear
%--

if log_is_open(log)
    disp(['Close log ''', log_name(log), ''' before emptying trash.']); return;
end

if isempty(log.deleted_event)
    return;
end

%--
% clear deleted events and save log
%--

log.deleted_event = empty(event_create);

log_save(log);

