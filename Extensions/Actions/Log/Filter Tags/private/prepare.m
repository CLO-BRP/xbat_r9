function [result, context] = prepare(parameter, context)
% FILTER TAGS - prepare

result = struct;
tstart = tic;
% action
% all_tags
% tag_list
% empty_logs

%Stop if any open browsers
if close_browsers
    return;
end

%retrieve list of selected logs
logs = get_logs( context );
if isempty( logs )
    fail( 'API supplied no context.target in prepare.m.', 'WARNING' );
    return;
end

%create waitbar
waitbar_name = sprintf( 'XBAT_WAITBAR::%s%s', string_ing( context.ext.name ), ' Logs ...' );
wh = findobj( 'tag', waitbar_name );

%retrieve list of sound from which logs were selected
sound_names = cellfun( @fileparts, logs, 'UniformOutput', false );
% unique_snd_names = unique( sound_names );

%create empty event
e_empty = empty( event_create );
    
%---------------------------------------------------------------------
% for each log
%---------------------------------------------------------------------
NumLogs = length( logs );
for i = 1:NumLogs
    
    %initializations
    e_cum = e_empty;

    %get sound struct for sound corresponding to log
    curr_sound_name = sound_names{ i };
    out = sound_load( context.library, curr_sound_name );
    context.sound = out.sound;
    
    %indicate log being processed in waitbar
    t0 = tic;
    waitbar_update( wh(1), 'PROGRESS', 'message', ['Processing ', logs{ i }, ' ...'] );

    %load log, refresh file name and path
    curr_log = logs{ i };
    [ log, snd_name, status ] = load_log( curr_log, context );
    if status
        return;
    end
    
    %go to next input log if no events and user doesn't want empty output logs
    if ~log.length && ~parameter.empty_logs
       continue; 
    end
    
    %---------------------------------------------------------------------
    %make list of tags in events
    %---------------------------------------------------------------------
    
    [ ~, fn ] = fileparts( curr_log );
    e = log.event;
    
    %get event tags
    tags = get_tags( e );
    
    % warn if event has multiple tags
    idx = cellfun( @length, tags, 'UniformOutput', false );
    idx = find( [ idx{ : } ] > 1 );
    if ~isempty( idx )
        d1 = sprintf( '%s\n has multiple tags in events\n', log_name( log ) );
        d2 = sprintf( '  %.0f\n', e( idx ).id );
        fail( sprintf( '%s%s', d1, d2 ), 'WARNING' )
%         error( 'No error' )
    end
    
    %set empty tags to '(NoTag)'
    mask = cellfun( @isempty, tags, 'UniformOutput', false );
    tags( [ mask{ : } ] ) = { { '(NoTag)' } };
    
    % select primary tag for each event
    tags = cellfun( @(x) x{ 1 }, tags, 'UniformOutput', false );
    
    %---------------------------------------------------------------------
    %make list of tags to filter
    %---------------------------------------------------------------------
    
    %if user wants to use all tags, make a list of all tags in log
    if strcmp( parameter.all_tags, 'Copy events with all tags' )
        tag_list = unique( tags );
        
    %if user wants to specify tag list, use list of tags supplied by user
    elseif strcmp( parameter.all_tags, 'Copy events with tags listed below' )
        tag_list = parameter.tag_list;
    end
    
    %for each selected tag
    for curr_tag = tag_list'
        
        %go to next tag if no events with selected tag and user doesn't
        %want empty output logs
        mask = ismember( tags, curr_tag );
        if ~any( mask )
            if parameter.empty_logs
                e_curr = e_empty;
            else
                continue;
            end
        else
            
            %filter events with tag
            e_curr = e( mask );

            %set current tag into event (unset and blank tags set to "(NoTag)")
            e_curr = set_tags( e_curr, curr_tag );
        end               
        
        %if user wants single-tag logs, make a write log
        if strcmp( parameter.action, 'Separate events into single-tag logs' )
            make_log( fn, curr_tag, e_curr, context );
            
        %if user wants multi-tag logs, save events
        else
            e_cum = [ e_cum, e_curr ];
        end
    end
         
    %if user wants multi-tag logs, write log
    if strcmp( parameter.action, 'Combine events into multi-tag logs' )
        
        %if log is empty, create log only if user wants empty logs
        if ~isempty( e_cum ) || parameter.empty_logs
            make_log( fn, tag_list, e_cum, context );
        end
    end

    %display log processed in waitbar
    result.target = log;
    result.status = 'done';
    result.message = '';
    result.error = [];
    result.elapsed = toc( t0 );
    result.output = struct( [] );
    action_waitbar_update( wh(1), i/NumLogs, result, 'log' ); 
    
end

d = sprintf( '%.0f logs processed in %f sec', NumLogs, toc(tstart) );
waitbar_update( wh(1), 'PROGRESS', 'message', d );

%------------------------------------------------------------------------
% stop execution to prevent risk of memory leak in compute.
%--------------------------------------------------------------------------
error( 'No Error' )  %To avoid "Prepare failed" dialog, us new action_dispatch.m

%------
function make_log( fn, tags, e, context )

    %stop execution of output log already exists
    tag_str = tags_to_str( tags );
    tag_str = strrep( tag_str, ' ', '_' );
    fn = sprintf( '%s_%s', fn, tag_str );
    fn_full = fullfile( context.library.path, sound_name( context.sound ), 'Logs', [ fn, '.mat' ] );
    if exist( fn_full, 'file' )
        d = sprintf( 'Log already exists: %s', fn );
        fail( d, 'WARNING' );
        error( 'No Error' )
    end
    
    %create new empty log
    log = new_log( fn, context.user, context.library, context.sound );
    
    %add events to log
    log.event = e;
    log.length = length( e );
    log.curr_id = max( [ e( : ).id ] );
    
    %save events into log
    log_save( log );








