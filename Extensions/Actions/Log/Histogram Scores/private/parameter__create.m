function parameter = parameter__create(context)

% HISTOGRAM SCORES - parameter__create

parameter = struct;

parameter.MaxScore = [];
parameter.SqrtScore = [];
parameter.SeparateChannels = [];
parameter.OutputFrequencies = [];
parameter.inc = 0.05;