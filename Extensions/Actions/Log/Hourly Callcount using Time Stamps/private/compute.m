function [result, context] = compute(log, parameter, context)
result = struct;

% HOURLY PRESENCE - compute


%%
%Log Action "Hourly Presence"

%Creates CSV with hourly presence of events by channel

%%
if context.state.kill
  return;
end

if log_is_open(log)
    disp(['Close log ''', log_name(log), ...
      ' before calculating hourly presense.']); return;
end

%if user selected "refresh" (refresh_mode = 1) and current sound is same as last sound
if parameter.attribute_mode
    
    %set date-time attribute
    log.sound = set_date_time(log.sound);

    %set time-stamp attribute
    log.sound = set_time_stamps(log.sound);

end

%calculate date number of each event and save in context.state
len = length(log.event);
realtime = zeros(len, 1);
chan = zeros(len, 1);
for i = 1:len  
  mapped_time = map_time(log.sound, 'real', 'record', log.event(1,i).time(1));  
  realtime(i) = log.sound.realtime + mapped_time / 86400;  
  chan(i) = log.event(1,i).channel;
end
context.state.realtime = [context.state.realtime; realtime];
context.state.fn = [context.state.fn; log_name(log)];
context.state.chan = [context.state.chan, chan];

% if log_is_open(log)
%     disp(['Close log ''', log_name(log), ...
%       ' before calculating hourly presense.']); return;
% end
% 
% if ~iscell(context.state.sound.file)
%     
%     context.state.sound.file = {context.state.sound.file};
%     
% end
% 
% %if user selected "fast" mode and current sound is same as last sound
% if parameter.mode && strcmp(log.sound.file{1}, context.state.sound.file{1})
%     
%     log.sound = context.state.sound;
%     
% %get date-time and time-stamps attributes from sound file names
% else
%     
%     %set date-time attribute
%     log.sound = set_date_time(log.sound);
% 
%     %set time-stamp attribute
%     log.sound = set_time_stamps(log.sound);
% 
% end
% 
% %calculate date number of each event and save in context.state
% len = length(log.event);
% realtime = zeros(len, 1);
% 
% for i = 1:len
%   
%   mapped_time = map_time(log.sound, 'real', 'record', log.event(1,i).time(1));
%   
%   realtime(i) = log.sound.realtime + mapped_time / 86400;
%   
% end
% 
% context.state.realtime = [context.state.realtime; realtime];
% 
% context.state.fn = [context.state.fn; log_name(log)];
% 
% if parameter.mode
%     
%     context.state.sound = log.sound;
%     
% end