function [result, context] = conclude(parameter, context)
result = struct;

% HOURLY PRESENCE - conclude
if context.state.kill
  return;
end

% find first and last day in sound stream
sound_begin_time = file_datenum(context.sound.file{1});
sound_end_time = file_datenum(context.sound.file{end});
dur_last_sound_file = context.sound.samples(end)/context.sound.samplerate/86400;
sound_end_time = sound_end_time + dur_last_sound_file;
first_day = floor(sound_begin_time);
last_day = floor(sound_end_time);
num_days = last_day - first_day + 1;
num_hours = num_days * 24;

% make callcount matrix
realtime = context.state.realtime;
if isempty(realtime)
  fprintf(2,'No events to count.\n');
  return;
end

% make callcount matrix
chan = context.state.chan;
HHH = floor(24 * (realtime - first_day)) + 1;
count = zeros(num_hours, context.sound.channels);
for i = 1:length(HHH)
  count(HHH(i,1), chan(i,1)) = count(HHH(i,1), chan(i,1)) + 1;
end

% create matrix with output table title and header
fn = context.state.fn;
if ~iscell(fn)
    fn = {fn};
end
num_logs = length(fn);
M_title = cell(3 + num_logs, 3 + context.sound.channels);
M_title{1,1} = 'Hourly Callcount by Channel';
M_title{2,1} = '';
M_title{3,1} = 'Source Logs:';
M_title(3:num_logs+2,2) = fn;
M_header = cell(3, 3 + context.sound.channels);
M_header{1,3} = 'Channels-->';
M_header(2,3:2 + context.sound.channels) = cellstr(num2str([1:context.sound.channels]'))';
M_header{3,3 + context.sound.channels} = 'Hour Total';
M_header{3,1} = 'Date';
M_header{3,2} = 'Hour';

% create matrix with output table body
M_body = cellfun(@num2str, num2cell(count), 'UniformOutput', false);
count_sum = cellstr(num2str(sum(count, 2)));
M_body_sum = [M_body, count_sum];

% % IDX_hour = 1:max(HHH);
IDX_hour = 1:num_hours;
IDX_hour = IDX_hour' - 1;
date = cellstr(datestr(first_day + floor((IDX_hour)./24)));
hour = cellstr(num2str(mod(IDX_hour, 24)));
M = [date, hour, M_body_sum];

% stitch output together;
M = [M_title; M_header; M];

xls_name = sprintf('hourly_callcount_%s', datestr(now,30));

[xls_name, xls_path] = uiputfile( ...
  '*.xlsx', ...
  'Hourly Callcount using Time Stamps: output', ...
  xls_name);

if isequal(xls_name, 0)
  return;
end

% % xlswrite([xls_path '\' xls_name], line);
xlswrite(fullfile(xls_path, xls_name), M);

% % realtime = context.state.realtime;
% % 
% % first_day = floor(min(realtime));
% % 
% % last_day = floor(max(realtime));
% % 
% % num_days = last_day - first_day + 1;
% % 
% % count = zeros(num_days, 24);
% % 
% % fn = context.state.fn;
% % 
% % num_logs = size(fn, 1);
% % 
% % if isempty(realtime)
% %   fprintf(2,'No events to count.\n');
% %   return;
% % end
% % 
% % for i = 1:size(realtime, 1)
% %   
% %   day = floor(realtime(i) - first_day) + 1;
% %   
% %   hh = floor(24 * mod(realtime(i), 1)) + 1;
% % 
% %   count(day, hh) = count(day, hh) + 1;
% % 
% % end
% % 
% % line = cell(num_days + num_logs + 7, 27);
% % 
% % line(1,1) = {'Hourly Callcount'};
% % 
% % line(2,1) = {' '};
% % 
% % line(3,:) = {'Date','0h-1h','1h-2h','2h-3h','3h-4h','4h-5h','5h-6h','6h-7h',...
% %              '7h-8h','8h-9h','9h-10h','10h-11h','11h-12h','12h-13h',...
% %              '13h-14h','14h-15h','15h-16h','16h-17h','17h-18h','18h-19h',...
% %              '19h-20h','20h-21h','21h-22h','22h-23h','23h-24h','SUM','MaxHr'};
% %            
% % for i = 1:num_days
% % 
% %   line(i+3,1)    = {datestr(i + first_day - 1, 'mmm.dd,yyyy')};
% %   line(i+3,2:25) = num2cell(count(i, :));
% %   
% %   sum_count = sum(count(i,:));
% %   line(i+3,26)   = num2cell(sum_count);
% %   
% %   if sum_count
% %   
% %     MaxHr = max(count(i, :));
% %     MaxHrIDX = find(count(i, :) == MaxHr);
% %     lenMax = length(MaxHrIDX);
% % 
% %     line(i+3,27:26+lenMax) = line(3, MaxHrIDX  + 1);  
% %     
% %   end
% % end
% % 
% % line(i+4,1) = {' '};
% % 
% % ii = i + 5;
% % 
% % line(ii,1) = {'SUM'};
% % 
% % sum_count = zeros(1,24);
% % 
% % for j = 1:24
% %   sum_count(j) = sum(count(:,j));
% % %   line(ii,j+1) = {sum(count(:,j))};
% % end
% % 
% % for i = 1:24
% %   line{ii,i+1} = sum_count(i);
% % end
% % 
% % grand_sum_count = sum(sum_count);
% % % sum_count = sum(sum(count));
% % 
% % line(ii,26) = {grand_sum_count};
% %   
% %   if grand_sum_count
% %   
% %     MaxHr = max(sum_count);
% %     MaxHrIDX = find(sum_count == MaxHr);
% %     lenMax = length(MaxHrIDX);
% % 
% %     line(ii,27:26+lenMax) = line(3, MaxHrIDX  + 1);  
% %     
% %   end
% % 
% % line(ii+1,1) = {' '};
% % 
% % iii = ii + 2;
% % 
% % line(iii,1) = {'Logs counted:'};
% % 
% % for k = 1:num_logs
% %   line(iii + k,1) = fn(k);
% % end
% % 
% % xls_name = sprintf('callcount_%s', datestr(now,30));
% % 
% % [xls_name, xls_path] = uiputfile( ...
% %   '*.xlsx', ...
% %   'Hourly Callcount using Time Stamps: output', ...
% %   xls_name);
% % 
% % if isequal(xls_name, 0)
% %   return;
% % end
% % 
% % % % xlswrite([xls_path '\' xls_name], line);
% % xlswrite(fullfile(xls_path, xls_name), line, 'All Chan');

