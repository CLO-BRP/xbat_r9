function [result, context] = prepare(parameter, context)

result = struct;

% HOURLY PRESENCE - prepare

context.state.realtime = [];

context.state.fn = {};

%% REFRESH - prepare
%
%**************************************************************************
% THIS VERSION IS NOT CONFUSED BY HAVING MULTIPLE LOGS WITH THE SAME FILE
% NAME IN MULTIPLE XBAT LIBRARY FOLDERS
%**************************************************************************
%
% Refreshed structure variable name and file field for XBAT logs, using the
% file name of the log
%
% Allows XBAT actions to be run on XBAT logs whose names have been changed
% without having to first open them in XBAT. 
%

%initializations
context.state.kill = 0;
context.state.snd_lib_root = '';
% % context.state.sound.file = '';
context.state.chan = [];

%kill if multiple sounds selected
if ~isfield(context.sound, 'file')
	context.state.kill = 1;
	fail('Please check that a single sound is selected')
	return;
end

%do not refresh logs if refresh_mode = 0
if ~parameter.refresh_mode
    
    return;
    
end

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

% for each log
NumLogs = length(logs);
for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [SoundName fn] = fileparts(CurrentLog);
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;
    
    [NewLog.sound, context] = refresh_sound(NewLog.sound, fn, context);
    
    if context.state.kill
      return;
    end

    % save log
    log_save(NewLog);
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n');
    context.state.kill = 1;
    return;
  end
end
