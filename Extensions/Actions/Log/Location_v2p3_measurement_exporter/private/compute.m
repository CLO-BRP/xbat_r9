function [result, context] = compute(log, parameter, context)

% LOCATION_V2P3_MEASUREMENT_EXPORTER - compute

result = struct;

if context.state.kill
  
  return;
  
end


%% initializations

%find log name
fn = log_name(log);

%set Date-time attribute in log
log.sound = PRBA_set_date_time(log.sound);  

%set Time-stamps attribute in log
log.sound = PRBA_set_time_stamps(log.sound); 


%% Make mask to delete unwanted columns

mask = make_mask(parameter, log.sound.channels);

num_columns = sum(mask);


%% write column headers to cell array

column_headers = Location_v2p3_measurement_exporter('export_headers', log);

num_columns_original = size(column_headers,1);


%% write exported measurements to cell array

utc_offset = str2double(parameter.utc_offset);

M_values = cell(log.length,size(column_headers,1));

for i = 1:log.length

  try
    M_values(i,:) = Location_v2p3_measurement_exporter('export_values', ...
                                                       log, ...
                                                       i, ...
                                                       utc_offset, ...
                                                       num_columns_original);
  catch 
        % Warn the user that no values were exported. Measurement may be empty
        t = sprintf(['WARNING\n', ...
              '"%s"\n  has no valid CSE Location measurement in event #%.0f'], ...
              log_name(log), log.event(i).id);
        fprintf(2, '\n%s\n', t);        
  end
end


%% Delete unwated measurement columns

column_headers = column_headers(mask,1);

M_values = M_values(:,mask);


%% count number of XBAT logs processed

context.state.iteration = context.state.iteration + 1;

%% if summary output requested

if context.state.summary_output
  
  fnC(1:log.length,1) = {fn};
  
  snd_name(1:log.length,1) = {sound_name(log.sound)};
  
  M_values_summary = [fnC, snd_name, M_values];

  if context.state.iteration < 2

    % make header for summary worksheet
    sheet_header = cell(3,num_columns+2);
    sheet_header{1,1} = 'SUMMARY';
    
    summary_column_headers = [{'Log Name','Sound name'}, column_headers'];
    
    header = [sheet_header; summary_column_headers];

    context.state.M_summary = [header; M_values_summary];
    
  else

    context.state.M_summary = [context.state.M_summary; M_values_summary];
    
  end
  
end
 
%% if detailed output requested
if context.state.detailed_output
  
  % output XBAT sound attributes to XLS

  attributes = make_sensor_sheet(log);
  
  sheet_header = cell(3,(size(attributes,2)));
  sheet_header(1,1:3) = {'Log Name:','',fn};
%   sheet_header = make_header(size(attributes,2), fn);

  M2 = [sheet_header; attributes];

  sheet_name = sprintf('Sensors-Log%03.0f', context.state.iteration);

  % sheet_name = fn(end-min(30, length(fn)-1):end);

  xlswrite(context.state.OutPath, M2, sheet_name); 


  % output measurements to XLS

  sheet_header = cell(3,num_columns);
  sheet_header(1,1:3) = {'Log Name:','',fn};
%   sheet_header = make_header(num_columns, fn);

  M = [sheet_header; column_headers'; M_values];

  sheet_name = sprintf('Location_v2p3-Log%03.0f', context.state.iteration);

  % sheet_name = fn(end-min(30, length(fn)-1):end);

  xlswrite(context.state.OutPath, M, sheet_name); 

end
