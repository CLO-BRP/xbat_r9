function header = make_header(num_col, fn)


header = cell(3,num_col);

header{1,1} = 'Log Name:';

header{1,3} = fn;


% 
% header{1} = 'Location v2.2 Measurements';
% 
% header{2,1} = 'Log Name:';
% 
% header{2,3} = fn;
% 
% header{4,1} = 'UTC Offset:';
% 
% header{4,3} = parameter.utc_offset{1};


% header{4,1} = 'Time Run:';
% 
% header{4,4} = datestr(now, 21);
%
% header{6,1} = 'Geodetic Datum:';
% 
% header{6,4} = context.state.dtum;
% 
% header{7,1} = 'UTM Zone:';
% 
% header{7,4} = [num2str(context.state.zone), context.state.hem];
% 
% header{9,1} = 'Position of Local Reference Point';
% 
% header{10,2} = 'Easting (UTM)';
% 
% header{10,4} = context.state.utm_east;
% 
% header{11,2} = 'Northing (UTM)';
% 
% header{11,4} = context.state.utm_north;

