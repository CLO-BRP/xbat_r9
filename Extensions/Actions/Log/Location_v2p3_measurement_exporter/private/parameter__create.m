function parameter = parameter__create(context)

% LOCATION_V2P3_MEASUREMENT_EXPORTER - parameter__create

parameter = struct;


preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Log', ...
                       context.ext.name, ...
                       'private', ...
                       'preset3.mat');

if exist(preset_path,'file')
    
    load(preset_path)
    
else
   
%%
%% rename preset3.mat if any of these change !!!
%%


    parameter.id = 1;

    parameter.tag = 1;

    parameter.rating = 1;

    parameter.score = 1;

    parameter.ref_chan = 1;

    parameter.freq = 1;

    parameter.time = 1;
    

    parameter.excel_date = 1;

    parameter.time_str = 1;

    parameter.julian_day = 1;
    
    parameter.utc_offset = 8;
    
    
    parameter.location = 1;

    parameter.UTM = 1;

    parameter.lat_lon = 1;

    parameter.xyz_corr_sum = 1;

    parameter.all_locations = 1;

    parameter.cent_dist = 1;

    parameter.iterations = 1;

    parameter.channels_used = 1;

    parameter.peak_vals = 1;

    parameter.channels_wts = 1;

    parameter.peak_vals_sm = 1;

    parameter.peak_lags = 1;

    parameter.loc_lags = 1;

    parameter.dist2channels = 1;

    parameter.range = 1;

    parameter.bearing = 1;


    parameter.total_error = 1;

    parameter.statistical_error = 1;
      
    parameter.sound_speed_error = 1;
      
    parameter.cummulative_error_sensor_pos = 1;
    
    parameter.range_error = 1;

    parameter.bearing_error = 1;
    
    
    parameter.output_style = 1;
    

    save(preset_path,'parameter');
    
end

