function [result, context] = prepare(parameter, context)

% LOCATION_V2P3_MEASUREMENT_EXPORTER - prepare
%
% History
%   msp2 - 14 Apr 2014
%       Preserve log names with "." in them.
%   msp2 - 14 Nov 2014
%       Remove unneeded test that Mapping Toolbox is installed.
%  msp2 - 11 Mar 2016
%       Accommodate Z dimension

result = struct;

context.state.kill = 0;


%% Test if Mapping Toolbox is installed
toolboxes = ver;


%% make list of log names

% list names of selected logs
if isfield(context, 'target')   %check if context.target is available
  logs = context.target;
else
  fprintf(2,'API supplied no context.target in prepare.m \n');
  context.state.kill = 1;
  return;
end

if ~iscell(logs)
  logs = {logs};
end

NumLogs = length(logs);


%% stop execution if log is open in an XBAT browser

status = ASE_log_is_open(logs, NumLogs);

if status
  
  return;
  
end


%% ask user where to direct output

output_fn = ['Location_v2p3_', context.library.name, '.xlsx'];

% output_fn = ['Location_', ...
%              sound_name(context.sound), ...
%              datestr(now, '_yyyymmddTHHMMSSFFF')];

filter_spec = {'*.xls', '*.*'};

txt = 'Save output as';

ExtPath = fullfile( ...
  xbat_root, 'Extensions', 'Actions', 'Log', context.ext.name, 'private', 'preset.mat');

%recover last path selected for output, if available
if exist(ExtPath, 'file')
  
  load(ExtPath);
  
else

    OutPath = '';
  
end

OutPathFull = fullfile(OutPath, output_fn);

%ask user to select path for destination
[output_fn, OutPath] = uiputfile(filter_spec, txt, OutPathFull);

%trap selection of "Cancel" button
if isequal(OutPath, 0) || isequal(output_fn, 0)
  
  context.state.kill = 1;
  
  return;
  
end
    
OutPathFull = fullfile(OutPath, output_fn);

  
save(ExtPath, 'OutPath')



%push OutPath into context.state for use in compute.m
context.state.OutPath = OutPathFull;


%% refresh log name and path in log

for i = 1:NumLogs
  
  %determine new log name and display
  CurrentLog = logs{i};
  
  % load log structure
  [ SoundName, fn1, fn2 ] = fileparts( CurrentLog );
  fn = [ fn1, fn2 ]; %preserve log names which contain "."
  NewLog = get_library_logs('logs',[],SoundName,CurrentLog);
   
  %check if a single, valid log is specified
  if ischar(SoundName) && ischar(fn) && isequal(length(NewLog),1)
    
    % rename file field in log
    fnExt = [fn '.mat'];
    NewLog.file = fnExt;

    % rename path field in log
    LogPath = context.library.path;
    LogPath = [LogPath SoundName '\Logs\'];
    NewLog.path = LogPath;

    % save log
    log_save(NewLog);
    
    %stop execution if current log has different number of channels
    if isequal(i,1)
      num_chan = NewLog.sound.channels;
    elseif ~isequal(num_chan,NewLog.sound.channels)
      d = sprintf('Log has different number of channels than previous log:\n  %s',fn);
      fail(d, 'WARNING')
      context.state.kill = 1;
      return;
    end
    
  else
    fprintf(2,'API supplied no SoundName and fn in prepare.m, or get_library_logs returned invalid or multiple logs \n')
    context.state.kill = 1;
    return;
  end
end


%% Output header of summary worksheet, if requested

if iscell(parameter.output_style)
  output_style = parameter.output_style{1};
else
  output_style = parameter.output_style;
end

if strcmp(output_style, 'Summary Output')
  context.state.summary_output = 1;
  context.state.detailed_output = 0;
elseif strcmp(output_style, 'Detailed Output')
  context.state.summary_output = 0;
  context.state.detailed_output = 1;
else
  context.state.summary_output = 1;
  context.state.detailed_output = 1;
end

%% index for iterations of compute.m

context.state.iteration = 0;
context.state.M_summary = cell(0);
