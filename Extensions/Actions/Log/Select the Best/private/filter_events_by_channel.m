function new_event = filter_events_by_channel( event, KeepChan )
% filter events in log by channels specified in KeepChan

new_event = event;

if length( event ) < 1 || isempty( KeepChan )
    return;
end

new_event( ~ismember( [ new_event.channel ], KeepChan ) ) = [];