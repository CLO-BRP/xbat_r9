function parameter = parameter__create(context)

% SELECT THE BEST - parameter__create

% Writes a new log with the N highest-scoring events for each channel.
%   -Prompts user for N.
%   -Displays the number of events in the log, as well as the identity of
%    each channel with fewer than N events.
%   -adds "_INCOMPLETE" suffix to log name if any channel has < N events
%
% *****************   WARNING!   ***********************************
% Gets number of channels from log if available. If log was created with a
% sound that has never been opened, gets number of channels from
% context.sound. If the log has been created without ever opening the 
% sound, and is in the library for a sound with the wrong number of 
% channels, this software will not work correctly.
% ********************************************************************
%
%  TO DO
%
% 1. Select channels to use in parameter palette
% 2. Do not put "INCOMPLETE" suffix when less than N events if not in
%    selected channels are if user says skipping is OK.

% Michael Pitrick
% msp2@cornell.edu
% 16 September 2010

% To Do
%
% 1. 

% Change Log
%
% 1. Select channels to use in user parameter palette.  For unselected
% channels, insufficiency of events does not result in "INCOMPLETE" suffix.

% DISCLAIMER OF WARRANTIES

% THE SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS AND BRP MAKES NO 
% REPRESENTATIONS OR WARRANTIES (WRITTEN OR ORAL). TO THE MAXIMUM EXTENT  
% PERMITTED BY APPLICABLE LAW, BRP DISCLAIMS ALL WARRANTIES AND CONDITIONS,  
% EXPRESS OR IMPLIED, AS TO ANY MATTER WHATSOEVER AND TO ANY PERSON OR  
% ENTITY, INCLUDING, BUT NOT LIMITED TO, ALL IMPLIED WARRANTIES OF  
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND  
% NON-INFRINGEMENT OF THIRD PARTY RIGHTS AND THOSE ARISING FROM A COURSE OF  
% DEALING OR USAGE IN TRADE. NO WARRANTY IS MADE THAT ANY ERRORS OR DEFECTS  
% IN THE SOFTWARE WILL BE CORRECTED, OR THAT THE SOFTWARE WILL MEET YOUR  
% REQUIREMENTS.  END USERS SHALL NOT COPY OR REDISTRIBUTE THE SOFTWARE  
% WITHOUT WRITTEN PERMISSION FROM BRP. 
 
% LIMITATION OF LIABILITY 
 
% IN NO EVENT SHALL BRP OR ITS DIRECTORS, FACULTY, OR EMPLOYEES, BE LIABLE  
% FOR DAMAGES TO OR THROUGH YOU OR ANY OTHER PERSON OR ENTITY FOR BREACH  
% OF, ARISING UNDER, OR RELATED TO THIS AGREEMENT OR THE USE OF SOFTWARE OR  
% DOCUMENTATION PROVIDED HEREUNDER, UNDER ANY THEORY INCLUDING, BUT NOT  
% LIMITED TO, DIRECT, SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, OR  
% SIMILAR DAMAGES (INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF  
% BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION OR  
% DATA, OR ANY OTHER LOSS) WHETHER FORESEEABLE OR NOT, REGARDLESS OF THE  
% FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  
% LIABILITY OR OTHERWISE.

parameter.N = 10;

parameter.mode = {''};

parameter.mode_idx = 4;

parameter.KeepChan = [];
