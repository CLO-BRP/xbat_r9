function [result, context] = compute(log, parameter, context)

% UNDELETE - compute

result = struct;

% Undeleted previously deleted events

%--
% check if log is open and there are deleted events to clear
%--

if log_is_open(log)
    disp(['Close log ''', log_name(log), ' before emptying trash.']); return;
end

if isempty(log.deleted_event)
    return;
end

%--
% clear deleted events and save log
%--

%add deleted events to regular events
log.event = [log.event log.deleted_event];

%update length of log
log.length = log.length + length(log.deleted_event);

%delete deleted events
log.deleted_event = empty(event_create);

log_save(log);
