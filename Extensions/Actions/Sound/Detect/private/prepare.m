function [result, context] = prepare(parameter, context)

% DETECT - prepare

result = struct;

%Stop if any open browsers
if close_browsers
    context.state.kill = true;
else
    context.state.kill = false;
end