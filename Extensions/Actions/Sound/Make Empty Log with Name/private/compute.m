function [result, context] = compute(sound, parameter, context)
% Sound Action: MAKE EMPTY LOGS WITH NAME
%
% Creates an empty log with a specified name.  Action will be canceled if a log with that name already exists.
%
% Field:  Log Name
%   Use: Text field specifying the name of the new log.
%   Default Value:  %SOUND_NAME%_
%   Implementation Note:
%      The log name field supports the following substitution parameters...
%         %LIBRARY_NAME%
%         %SOUND_NAME%
%
% Field:  Event Color
%   Use: Select from list of colors (subset of all possible colors)
%   Default Value: Red
%
% This version is for general use.
%
% E Lynette Rayle  (based on code by Mike Pitzrick & Chris Pelkie)
% elr37@cornell.edu
% 15 Feb 2012

    
    result = struct;

    %--
    % initializations
    %--
    soundName = sound_name(sound);
    libraryName = context.library.name;
    log_name = parameter.logFileName;
    log_name = token_replace(log_name, '%', 'SOUND_NAME',soundName,'LIBRARY_NAME',libraryName);
    eventColor = color_to_rgb(parameter.eventColor);

    %--
    % Make sure log doesn't already exist; cancel action if it does
    %--
    logs = get_library_logs('info', [], sound);
    fn_w_Path = [soundName '\' log_name];
    if ismember(fn_w_Path, logs)
        %--
        % Warn user that log already exists
        %--
        txt = sprintf('Action canceled.\nLog already exists.');
        fprintf(2,'\n%s\n', txt);
        uiwait(warndlg(txt, 'ERROR','modal'));
        return;
    end        

    %--
    % Make Log 
    %--
    NewLog = new_log(log_name, context.user, context.library, sound);
    NewLog.event = empty(event_create);
    NewLog.color = eventColor;

    log_save(NewLog);
end

%-----------------------------------------------
% TOKEN_REPLACE
%-----------------------------------------------

function str = token_replace(str, sep, varargin)
    [field, value] = get_field_value(varargin);
    for k = 1:length(field)
        tok = [sep, field{k}, sep];
        str = strrep(str, tok, value{k});
    end
end