function [result, context] = compute(sound, parameter, context)

% RAVEN - CREATE LISTFILE WITH RANDOM HOURS - compute

result = struct;

if context.state.kill
    return;
end

%------------------------------------------------------------------------
% Check for compatibility of sound stream with this extension
%------------------------------------------------------------------------

%---
% check that recording is file stream
%---
if ~iscell( sound.file )
    d = 'Extension cannot process sounds represented by a single file.';
    fail( sprintf( '%s\n  %s', d, sound_name( sound ) ), 'WARNING' )
    context.state.kill = 1;
    return;
end

%---
% Check that recording is continuous (not duty cycled)
%---
table = get_schedule_from_files( sound, 'yyyymmdd_HHMMSS' );
if ~isequal( table, [ 0 0 ] );
    d = 'Extension cannot process duty cycled recordings.';
    fail( sprintf( '%s\n  %s', d, sound_name( sound ) ), 'WARNING' )
    context.state.kill = 1;
    return;
end    

%---
% Check that no files in streams have more than one hour of recording
%---
max_duration = max( sound.samples ./ sound.samplerate );
if max_duration > 3600
    disp( 'WARNING' )
    disp( 'This extension is intended to be used on recordings with:' )
    disp( '  -Sound files of no more than 1 hour duration' )
end
if ~iscell( sound.file ) || length( sound.file ) < 2
    disp( 'WARNING' )
    disp( 'This extension is intended to be used on recordings with:' )
    disp( '  -Multiple days' )
end

%------------------------------------------------------------------------
% Generate random sample
%------------------------------------------------------------------------
%---
% Find number of whole hours in sound stream
%---
begin_date = file_datenum( sound.file{ 1 } );
tolerance = eps( begin_date );
begin_date = floor( begin_date + tolerance );
end_date = floor( file_datenum( sound.file{ end } ) + ( 2 * tolerance ) );
num_hrs = 24 * ( end_date - begin_date + 1 );

%---
% Find list of hour blocks with 60 minutes of recording
%---

% find MATLAB datenum for the beginning of each sound file
hr = file_hour( sound.file );

% delete hours for which there is less than one hour or recording
dur = sound.samples ./ sound.samplerate;
cutoff = 3600;
for i = hr( end ):-1:hr( 1 )
    hr_idx = isequal( hr, i );
    hr_dur = sum( dur( hr_idx ) );
    if hr_dur < cutoff
        hr( hr_idx ) = [];
    end
end

%make list of unique hours in sound
unique_hr = unique( hr );

%---
% Make lists of N random hours
%---
idx = randperm( length( unique_hr ) );
rand_hr = unique_hr( idx );

%------------------------------------------------------------------------
% Output series of sample folders, each with N Raven listfiles representing randomly-selected hours
%------------------------------------------------------------------------

% initializations
sample_num_hrs = parameter.sample_num_hrs;
num_samples = ceil( num_hrs / sample_num_hrs );
snd_name = sound_name( sound );
path_out = fullfile( parameter.out_dir, snd_name );
if exist( path_out, 'dir' )
    fail( sprintf( 'Output path already exists:\n  %s', path_out ), 'WARNING' );
    context.state.kill = 1;
    return;
end

% for each sample
for i = 1:num_samples
    
    %---
    % make directory for first sample
    %---
    out_path = fullfile( path_out, sprintf( 'sample-%04.0f', i ) );
    if exist( out_path, 'dir' )
        d = 'Selected directory already exists.  Execution terminated.';
        fail( sprintf( '%s\n  %s', d, out_path ), 'WARNING' )
        context.state = 1;
        return;
    end
    mkdir( out_path )
    
    %---
    % Output list files for each sample
    %---
    first = ( i - 1 ) * sample_num_hrs + 1;
    last = min( i * sample_num_hrs, num_hrs );
    
    % for each hour in sample
    for j = first:last
try
        curr_hr = rand_hr( j );
catch
    keyboard;
end
        curr_hr_idx = ( hr == curr_hr );
        curr_sound_files = sound.file( curr_hr_idx );
        sound_path = fullfile( sound.path, curr_sound_files );
        sound_path_char = char( sound_path );
        fn = sprintf( '%s_sample-%04.0f_%02.0fh.listfile.txt', snd_name, j, curr_hr );
        path_out_full = fullfile( out_path, fn );
        casewrite( sound_path_char, path_out_full );
    end
end
