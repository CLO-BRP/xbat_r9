function parameter = parameter__create(context)

% RAVEN - CREATE LISTFILE WITH RANDOM HOURS - parameter__create

parameter = struct;
preset_path = fullfile(xbat_root, ...
                       'Extensions', ...
                       'Actions', ...
                       'Sound', ...
                       context.ext.name, ...
                       'private', ...
                       'preset.mat');
try    
    load( preset_path )
    
    %test if parameter fields have changed
    field = { 'out_dir'
              'sample_num_hrs' 
            };					 
    assert( all( isfield( parameter, field )));    
catch
    parameter.sample_num_hrs = 10;
    parameter.out_dir = pwd;
	save( preset_path, 'parameter' );
end
