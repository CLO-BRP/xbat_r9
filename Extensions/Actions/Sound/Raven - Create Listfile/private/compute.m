function [result, context] = compute(sound, parameter, context)

% RAVEN - CREATE RAVEN LISTFILE - compute

result = struct;

if context.state.kill
    return;
end

%---
% Create output directory for current sound
%---
snd_name = sound_name( sound );

path_out = parameter.out_dir;
% path_out = fullfile( parameter.out_dir, snd_name );
% if exist( path_out, 'dir' )
%     fail( sprintf( 'Output path already exists:\n  %s', path_out ), 'WARNING' );
%     context.state.kill = 1;
%     return;
% end
% mkdir( path_out )

%---
% Output list file for current sound
%---
sound_path = fullfile( sound.path, sound.file );
sound_path_char = char( sound_path );
path_out_full = fullfile( path_out, [ snd_name, '.listfile.txt' ] );
casewrite( sound_path_char, path_out_full );
