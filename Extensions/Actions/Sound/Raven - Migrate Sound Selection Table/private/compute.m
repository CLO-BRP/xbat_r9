function [result, context] = compute(sound, parameter, context)

% RAVEN - MIGRATE SOUND SELECTION TABLE - compute

% Changes Begin Time and End Time in Raven Sound Selection Table so that it
% can be used with a different sound stream.
%
% Input
%   Raven Sound Selection Tables
%
% Outputs
%   Raven Sound Selection Tables with updated Begin Time and End Time
%   Raven Listfile corresponding to output Raven Sound Selection Tables
%
% Note that Outputs are collated into folders named after the folder that
% the sound files are in.

% History
%   msp2  9 Apr 2015
%       Add file extension to "Begin File" values.
%       Renumber selections when merging tables.
%   msp2  3 Dec 2015
%       Add option to sort selection tables by "Begin Time"
%   msp2 12 Oct 2016
%       Add option to get file offset from "Begin File" (useful in case
%       where selection tables were made with single sound files)

%---
% Initializations
%---
result = struct;

if context.state.kill
    return;
end

table_merged = {};

path_out = context.state.path_out;
path_tsv = context.state.path_tsv;
fn_tsv = context.state.fn_tsv;
if ~iscell( fn_tsv )
    fn_tsv = { fn_tsv };
end

%---
% Create output directory for current sound
%---
snd_name = sound_name( sound );
path_out = fullfile( path_out, snd_name );
if exist( path_out, 'dir' )
    fail( sprintf( 'Output path already exists:\n  %s', path_out ), 'WARNING' );
    context.state.kill = 1;
    return;
end
mkdir( path_out )

%---
% Output list file for current sound
%---
sound_path = fullfile( sound.path, sound.file );
sound_path_char = char( sound_path );
path_out_full = fullfile( path_out, [ snd_name, '.listfile.txt' ] );
casewrite( sound_path_char, path_out_full );

%---
% For each selected Raven Sound Selection Table
%---
for i = 1 : length( fn_tsv )

    %---
    % Read Raven Sound Selection Table
    %---
    try
        [headers,body] = read_selection_table(fullfile(path_tsv, fn_tsv{i}));
        table = [headers ; body];
    catch
      fail( 'The selected Raven Sound Selection Tables could not be read.', 'WARNING' )
      context.state.kill = 1;
      return;  
    end

    %---
    % Migrate Raven Sound Selection Table by updating Begin Time and End
    % Time to make compatible with new sound stream
    %---
    [ table_out, headers, selection_idx, status ] = migrate_table( table, sound, parameter );
    if status
        fail(sprintf('Selection table could not be migrated:\n  %s', fn_tsv{i}))
        context.state.kill = 1;
        continue;
    end
    
    %---
    % If user chooses, output migrated Raven Sound Selection Table
    %---
    if ~parameter.combine_tables
        
        %---
        % If user chooses, sort selections by Begin Time
        %---
        if parameter.sort_selections
            begin_time_str = table_out( :, 4 );
            begin_time = str2double(begin_time_str);
            [ ~, idx ] = sort( begin_time );
            table_out( :, : ) = table_out( idx, : );
        end
        
        %---
        % Output selection table
        %---
        table_out = [ headers ; table_out ];
        [ ~, fn_tsv2 ] = fileparts ( fn_tsv{ i } );
        fn = strtok( fn_tsv2, '.' );
        fn_out = sprintf( '%s_(migrated_to)_%s.selections.txt', fn, snd_name );
        fn_out_full = fullfile( path_out, fn_out );
        write_selection_table(table_out, fn_out_full)
        
    %---
    % Otherwise, merge all tables for this sound
    %---
    else
        
        %---
        % If headers don't match, stop execution
        %---
        if isempty(context.state.first_headers)
            first_headers = headers;
            context.state.first_headers = first_headers;
        else
            first_headers = context.state.first_headers;
            if ~isequal(size(headers), size(first_headers)) || ~all( strcmp( headers, first_headers ) )
                fail( sprintf('Headers in %s are not the same as in previous tables', fn_tsv{ i } ));
                context.state.kill = 1;
                return;
            end
        end
        
        %---
        % Merge selection table with previous selection tables
        %---
        table_merged = [ table_merged ; table_out ];
    end
end

%---
% If user chooses, output merged Raven Sound Selection Table
%---
if parameter.combine_tables
    
    %---
    % If user chooses, sort selections by Begin Time
    %---
    if parameter.sort_selections
        begin_time_str = table_merged( :, 4 );
        begin_time = str2double(begin_time_str);
        [ ~, idx ] = sort( begin_time );
        table_merged( :, : ) = table_merged( idx, : );
    end
    
    %---
    % Renumber selections
    %---
    len = size( table_merged, 1 );
    if len > 1
        id = 1:len;
        id_mat = num2str( id', 17 );
        id_cell = cellstr( id_mat );
        table_merged( :, 1 ) = id_cell;
    end
    
    %---
    % find name and path of output
    %---
    table_merged = [headers ; table_merged];
    fn_out = sprintf( 'mergedST_(migrated_to)_%s.selections.txt', snd_name );
    fn_out_full = fullfile( path_out, fn_out );
    write_selection_table(table_merged, fn_out_full)
end
