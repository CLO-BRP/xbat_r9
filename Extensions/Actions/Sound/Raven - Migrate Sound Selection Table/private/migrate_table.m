function [body, headers, selection_id, status] = migrate_table(table, sound, parameter)

% Migrate Raven Sound Selection Table by updating Begin Time and End Time 
% to make compatible with new sound stream
%
% Inputs
%   table = cell matrix containing original Sound Selection Table
%   sound = XBAT sound struct
%   parameter = user controls
%
% Outputs
%   body = cell matrix with body of output selection table
%   headers = cell vector with column headers of output selection table
%   selection_id = original selection id's in selection table
%   status = 0 if success, 1 if fail

% Note: Cannot use tdfread due to tabs at end of line.

% History
%   msp2  9 Apr 2015
%       Return index of "Selection" column.  
%       Expect "Begin File" values to have file extension.

%---
% Initializations
%---
status = 1;
body = {};
headers = {};
selection_id = {};

%---
% Convert "Begin Time", "End Time", "Begin File" and "File Offset (s)" columns into vectors
%---
headers = table(1,:);
selection_idx = strcmp( headers, 'Selection' );
begin_time_idx = strcmp( headers, 'Begin Time (s)' );
end_time_idx = strcmp( headers, 'End Time (s)' );
begin_file_idx = strcmp( headers, 'Begin File' );

switch parameter.source_file_offset{1}
    case 'File Offset (s)'
        file_offset_idx = strcmp( headers, 'File Offset (s)' );    
    case 'Begin File (s)'
        file_offset_idx = strcmp( headers, 'Begin Time (s)' );
    case other
        fail(sprint('"%s" is not a valid choice.', parameter.source_file_offset{1}, 'CODING ERROR'))
        return;
end

m = size(table,1);
if  m >= 2
    body = table(2:end,:);
    m = m - 1;
else
    return;
end
selection_id = body(:, selection_idx);
if isempty(selection_id)
    fail(sprintf('No selection id column found.'))
    return;
end
begin_time_str = body( :, begin_time_idx );
if isempty(begin_time_str)
    fail(sprintf('No selection id column found.'))
    return;
end
begin_time = str2double( begin_time_str );
if isempty(begin_time)
    fail(sprintf('No selection id column found.'))
    return;
end
end_time_str = body( :, end_time_idx );
if isempty(end_time_str)
    fail(sprintf('No selection id column found.'))
    return;
end
end_time = str2double( end_time_str );
if isempty(end_time)
    fail(sprintf('No selection id column found.'))
    return;
end
begin_file = body( :, begin_file_idx );
if isempty(begin_file)
    fail(sprintf('No selection id column found.'))
    return;
end
file_offset_str = body( :, file_offset_idx );
if isempty(file_offset_str)
    fail(sprintf('No selection id column found.'))
    return;
end
file_offset = str2double( file_offset_str );
if isempty(file_offset)
    fail(sprintf('No selection id column found.'))
    return;
end

%---
% Make vectors of XBAT file stream and cumulative time for XBAT file stream
%---
xbat_file_stream = sound.file;
% % [~, xbat_file_stream ] = cellfun( @fileparts, sound.file, 'UniformOutput', 0 );
if length( sound.cumulative ) > 1
    
    % convert cumulative number of samples to cumulative number of seconds
    cumulative_file_time = sound.cumulative ./ sound.samplerate;
    
    % find number of seconds of recording before each file in stream
    time_before = [ 0 ; cumulative_file_time( 1:end-1 ) ];
else
    time_before = 0;
end

%---
% Calculate new "Begin Time" and "End Time" for each selection
%--
new_begin_time = zeros( m, 1 );
new_end_time = new_begin_time;
j = 0;
skipped_lines = [];
for i = 1:m
   
    % find begin file in XBAT sound file stream
    idx = strcmp( xbat_file_stream, begin_file{ i } );
    
    % if no matching sound file found, skip selection
    if ~any( idx )
        skipped_lines = [ skipped_lines ; i ];
        continue;
    end
    
    % find number of seconds in file stream before begin file
    file_time = time_before( idx );
    
    % calculate new Begin Time
    curr_begin_time = file_time + file_offset( i );
    curr_end_time = curr_begin_time + diff( [ begin_time( i ), end_time( i ) ] );
    
    % put times in vector
    j = j + 1;
    new_begin_time( j ) = curr_begin_time;
    new_end_time( j ) = curr_end_time;    
end

% trim skipped lines from "Begin Time" and "End Time" vectors
new_begin_time = new_begin_time( 1:j );
new_end_time = new_end_time( 1:j );

%---
% If any lines weren't skipped, replace "Begin Time" and "End Time" columns in table
%---
if ~isempty( new_begin_time )
    
    % If any lines were skipped, delete them from output table
    if ~isempty( skipped_lines )
        body( skipped_lines, : ) = [];
    end
    
    % replace "Begin Time" and "End Time" columns in table
    new_begin_time_str = num2str( new_begin_time, 17 );
    new_begin_time_cell = cellstr( new_begin_time_str );
    body( :, begin_time_idx ) = new_begin_time_cell;
    new_end_time_str = num2str( new_end_time, 17 );
    new_end_time_cell = cellstr( new_end_time_str );
    body( :, end_time_idx ) = new_end_time_cell;
    
%---
% If all lines were skipped, table is empty 
else
    body = {};
end

status = 0;
