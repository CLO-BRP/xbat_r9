function parameter = parameter__create(context)

% RAVEN - MIGRATE SOUND SELECTION TABLE - parameter__create

parameter = struct;

parameter.combine_tables = 1;

parameter.sort_selections = 1;

parameter.source_file_offset = {''};