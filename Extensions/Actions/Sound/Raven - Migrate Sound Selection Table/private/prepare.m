function [result, context] = prepare(parameter, context)

% RAVEN - MIGRATE SOUND SELECTION TABLE - prepare

%---
% initializations
%---
result = struct;
context.state.kill = 0;

%---
% Select Raven Sound Selection Tables
%---
d = 'Select Raven Sound Selection Tables';
% % desktop = winqueryreg( 'HKEY_CURRENT_USER', 'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders', 'Desktop' );
% % pathCSV = fullfile( desktop, '*.txt' );
pathCSV = fullfile( pwd, '*.txt' );
[ fn_tsv, path_tsv ] = uigetfile( pathCSV, d, 'MultiSelect', 'on' );

% stop processing if no files selected
if isequal( fn_tsv, 0 ) || isequal( path_tsv, 0 )
	fail( 'No Raven Sound Selection Tables selected.', 'Cancel' )
    context.state.kill = 1;
	return;  
end

%---
% Select output path
%---
% % path_out = uigetdir( desktop, 'Select Folder for Output' );
path_out = uigetdir( pwd, 'Select Folder for Output' );

% stop processing if no files selected
if isequal( path_out, 0 )
	fail( 'User canceled.', 'Cancel' )
    context.state.kill = 1;
	return;  
end

%---
% Save names of input files, input path, and output path
%---
context.state.fn_tsv = fn_tsv;
context.state.path_tsv = path_tsv;
context.state.path_out = path_out;
context.state.first_headers = {};
