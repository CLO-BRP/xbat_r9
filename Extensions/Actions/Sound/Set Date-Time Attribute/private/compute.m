function [result, context] = compute(sound, ~, context)
result = struct;

% SET DATE-TIME ATTRIBUTE - compute
%
% Sound Action sets Date-Time attribute using time stamps in sound file
% names
%
% This action consists of modified XBAT code activated with
%   XBAT palette > Attributes > Add Attribute > Date Time
% which was written by Harold Figueroa (hkf1@cornell.edu) and 
% Matthew Robbins (mer34@cornell.edu)
%
% Michael Pitzrick
% msp2@cornell.edu
% 2008-12-23 - Create
% 2021-02-09 - Make compatible with XBAT "file" type sound

%list AIFF's in sound file stream
fnExt = sound.file;

%select first file in file stream
if iscell(fnExt)
  fnExt = fnExt{1};
end
[~,fn,~] = fileparts(fnExt);

%calculate datenum from time stamp in name of first AIFF
num = file_datenum(fn);

if isempty(num)
  
  txt = 'Unable to read time stamp in sound file names in sound:';
  
  txt2 = sprintf('%s\n  %s\n', txt, sound_name(sound));
  
  fprintf(2,'\nERROR:\n%s\n', txt2);
  
  h = warndlg(txt2, 'ERROR');
  
  movegui(h,'center')
  
  return;
  
end

if strcmp(sound.type,'file')
    attributeRoot0 = fullfile(context.library.path,fn,'Attributes');
else
    attributeRoot0 = fullfile(sound.path,'__XBAT','Attributes');
end

% if ~isempty(num)
%   set_control(callback.pal.handle, 'datetime', 'string', datestr(num))
% end

lines{1} = 'Date and Time';
lines{2} = '(MATLAB date vector)';

%convert datenum of first AIFF into date vector
vec = datevec(num);

%convert date vector into CSV string
line = '';
for k = 1:length(vec)
	line = [line, num2str(vec(k)), ', '];
end
line(end-1:end) = '';
lines{3} = line;

% if no 'Attributes' directory, then create
if ~exist(attributeRoot0,'dir')
    mkdir(attributeRoot0)
end

%write new date-time.csv
store = fullfile(attributeRoot0,'date_time.csv');
file_writelines(store, lines);
