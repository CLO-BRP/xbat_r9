function result = parameter__control__callback(callback, context)

% SET PARAMETERS - parameter__control__callback

result = struct;

%-------------------------------------------------------------------------
% update saved user parameter settings
%-------------------------------------------------------------------------

%determine preset path
ext_path = mfilename('fullpath');
ext_path = fileparts( ext_path );
preset_path = fullfile( ext_path, 'preset.mat' );

%load prest
load( preset_path )

%-------------------------------------------------------------------------
%process parameters for various ui control styles
%-------------------------------------------------------------------------
name = callback.control.name;
style = get( callback.control.handle, 'style' );
	
if any( strcmp(style, { 'slider' 'checkbox' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name ' = value;' ] );

elseif any( strcmp( style, { 'popupmenu' 'listbox' } ) )
  value = get( callback.control.handle, 'Value' );
  eval( [ 'parameter.' name '_idx = value;' ] );
  
elseif any( strcmp( style, { 'edit' } ) )
  value = get( callback.control.handle, 'String' );
  
  %update slider if edit is part of slider control
  if length( callback.control.handles ) > 1
    style2 = get( callback.control.handles( 2 ), 'style' );    
    if strcmp( style2, 'slider')
      value = str2double( value );
    end    
  end  
  eval( [ 'parameter.' name ' = value;' ] );  
end

%-------------------------------------------------------------------------
% enable/disable browser position controls
%-------------------------------------------------------------------------
if strcmp( name, 'position_enable' )
  
  position_enable = get_control(callback.pal.handle, 'position_enable', 'value');

  if position_enable
    set_control( callback.pal.handle, 'p1', 'enable', 1 );   
    set_control( callback.pal.handle, 'p2', 'enable', 1 );   
    set_control( callback.pal.handle, 'p3', 'enable', 1 );     
    set_control( callback.pal.handle, 'p4', 'enable', 1 );
  else
    set_control( callback.pal.handle, 'p1', 'enable', 0 );   
    set_control( callback.pal.handle, 'p2', 'enable', 0 );   
    set_control( callback.pal.handle, 'p3', 'enable', 0 );     
    set_control( callback.pal.handle, 'p4', 'enable', 0 );
    
  end
end

%-------------------------------------------------------------------------
% make sure browser width and height are positive
%-------------------------------------------------------------------------
if any( strcmp( style, { 'slider' 'edit' } ) )
  
  p1 = get_control( callback.pal.handle, 'p1', 'value' );
  p2 = get_control( callback.pal.handle, 'p2', 'value' );
  p3 = get_control( callback.pal.handle, 'p3', 'value' );
  p4 = get_control( callback.pal.handle, 'p4', 'value' );

  if p3 < p1
    temp = p3;
    p3 = p1;
    p1 = temp;
  end

  if p4 < p2
    temp = p4;
    p4 = p2;
    p2 = temp;
  end

  min_window_size = 1;
  p3 = max( p3, p1 + min_window_size );
  p4 = max( p4, p2 + min_window_size );

  set_control( callback.pal.handle, 'p1', 'value', p1 );
  set_control( callback.pal.handle, 'p2', 'value', p2 );
  set_control( callback.pal.handle, 'p3', 'value', p3 );
  set_control( callback.pal.handle, 'p4', 'value', p4 );
    
end

%-------------------------------------------------------------------------
% enable/disable "One-click" event set up
%-------------------------------------------------------------------------
if strcmp( name, 'oneclick_enable' )
  
  oneclick_enable = get_control(callback.pal.handle, 'oneclick_enable', 'value');

  if oneclick_enable
    set_control( callback.pal.handle, 'event_duration', 'enable', 1 );   
    set_control( callback.pal.handle, 'event_minfreq',  'enable', 1 );   
    set_control( callback.pal.handle, 'event_maxfreq',  'enable', 1 );
    set_control( callback.pal.handle, 'event_tag',      'enable', 1 );
  else
    set_control( callback.pal.handle, 'event_duration', 'enable', 0 );   
    set_control( callback.pal.handle, 'event_minfreq',  'enable', 0 );   
    set_control( callback.pal.handle, 'event_maxfreq',  'enable', 0 );
    set_control( callback.pal.handle, 'event_tag',      'enable', 0 );    
  end
end

%-------------------------------------------------------------------------
% make sure "One-click" upper frequency higher than lower frequency
%-------------------------------------------------------------------------
if any( strcmp( style, { 'slider' 'edit' } ) )
  
  event_minfreq = get_control( callback.pal.handle, 'event_minfreq', 'value' );
  event_maxfreq = get_control( callback.pal.handle, 'event_maxfreq', 'value' );

  if event_maxfreq < event_minfreq
    temp = event_maxfreq;
    event_maxfreq = event_minfreq;
    event_minfreq = temp;
  end

  set_control( callback.pal.handle, 'event_minfreq', 'value', event_minfreq );
  set_control( callback.pal.handle, 'event_maxfreq', 'value', event_maxfreq );
    
end

%save preset
save(preset_path,'parameter');