function [result, context] = compute(sound, parameter, context)
%
% SET TIME STAMPS ATTRIBUTE - compute
%
% Adds time stamps to a sound; runs on a batch of Sounds
%  in contrast to an earlier Attribute action that needed to
%  be run by hand on each sound
%
% Also sets the Hide Silence option to ON (1) for each sound
% However, this last flag is set for a sound in a user library
%  so does not travel with the sound itself, that is, best
%  that the user run this batch operation on all the sounds once
%  they are loaded into her library
%
% Chris Pelkie 2011-01-31
%  building on prototype by Ann Warde 2011-01
%  stealing lots of ideas from Mike Pitzrick's
%   code for Set Date-Time Attribute
%
% CRP 2011-02-28
%  added the line to explicitly add the time_stamp.table to
%  the Sound; I had thought this was done automagically by XBAT
%  by we kept getting errors related to it not being there
%  when a new sound to which this action had just been applied
%  was opened; this fix seems to stop that
%
% Michael Pitzrick - 18 September 2012
%  Improvement to file_times_from_names means that repairing
%  time_stamps.table is no longer necessary, so this code is removed. 
%  Until file_times_from_names makes it into XBAT general release, the
%  improved file_times_from_names and get_schedule_from_files must be in
%  the private directory for this extension

result = struct;


%--- calculate time stamps table
table = get_schedule_from_files(sound, parameter.DatePattern);
  
sound.time_stamps.table = table;

flag = 0;

for i = 1:length(sound.attributes)
  
  if strcmpi(sound.attributes(i).name, 'time_stamps')
    
    sound.attributes(i).value.table = table;
    
    flag = 1;
    
  end
  
end

if ~flag
  
  time_stamp.name = 'time_stamps';
  
  time_stamp.value.table = table; 
  
  sound.attributes = [sound.attributes, time_stamp];
  
end


%enable time stamps attribute and hide

sound.time_stamp.enable   = 1;

sound.time_stamp.collapse = 1;


% convert time stamp table to CSV strings
len = size(table, 1);

% preallocate cell array
lines = cell(len+2, 1);

% write literal header lines
lines{1} = 'sound time, stamp time';

lines{2} = 'stamps';

% write strings for CSV comma-delimited format

for i = 1:len
  
    lines{i + 2} = ...
        [num2str(table(i, 1),'%.7f'), ', ' num2str(table(i, 2),'%.7f')];
%     lines{i + 2} = ...
%         [num2str(table(i, 1),'%.17f'), ', ' num2str(table(i, 2),'%.17f')];
      
end


% create sound attributes directory, if does not exist

outpath = fullfile(sound.path, '__XBAT', 'Attributes');

if ~exist(outpath, 'dir')
  
    mkdir(outpath);
    
end


% write time_stamps.csv

store = fullfile(sound.path, '__XBAT', 'Attributes', 'time_stamps.csv');

file_writelines(store, lines);


% update sound struct in XBAT library

sound.time_stamp.table = table;

sound_save(context.library, sound, get_sound_state(sound, context.library));

